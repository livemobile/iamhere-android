package com.mmtechco.iamhere.sms;

import com.mmtechco.iamhere.Driver;
import com.mmtechco.iamhere.R;
import com.mmtechco.iamhere.contacts.ContactsResource;
import com.mmtechco.iamhere.data.DataManager;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.telephony.SmsMessage;

public class SMSPlaces extends BroadcastReceiver
{

	@Override
	public void onReceive(Context context, Intent intent) 
	{
        Bundle bundle = intent.getExtras();
        
        if (bundle != null)
        {
            Object[] pdus = (Object[]) bundle.get("pdus");
            
            SmsMessage msg = SmsMessage.createFromPdu((byte[])pdus[pdus.length - 1]);
            
            String body = msg.getMessageBody();
            
            if(body.contains(Driver.getIamhereURL()))
            {
            	try
            	{
            	DataManager dataStore = new DataManager(context);
            	String[] massageParts = body.split(context.getString(R.string.TAG_PLACE));
            			 massageParts = massageParts[1].split(context.getString(R.string.TAG_URL));
            			 
            	String placeName = massageParts[0].trim();
            	//Log.v("987654345678", placeName);
            			 massageParts = massageParts[1].split(Driver.getIamhereURL())[1].split(",");
       			 
            			 
            	if(dataStore.placeExists(placeName))		 
            	{
            		ContactsResource contact = ContactsResource.getInstance(context, false);
            		String name = contact.getContactNameFromNumber(msg.getOriginatingAddress());
            		placeName = placeName+'('+name+')';
            		if(dataStore.placeExists(placeName))
            		{
                		dataStore.updateEntry(massageParts[0].replace('?', ' ').trim(), 
	            							  massageParts[1].trim(), 
	            							  placeName);
            			return;
            		}
            	}
            	
        		dataStore.addEntry(massageParts[0].replace('?', ' ').trim(), 
						  massageParts[1].trim(), 
						  placeName);
        		
            	}
            	catch(ArrayIndexOutOfBoundsException e)
            	{
            		
            	}     
            }
        }      
	}
}