package com.mmtechco.iamhere.sms;

import com.mmtechco.iamhere.Driver;
import com.mmtechco.iamhere.R;
import com.mmtechco.iamhere.contacts.ContactTarget;

import android.app.PendingIntent;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.os.Handler;
import android.os.Message;
import android.telephony.SmsManager;
import android.widget.Toast;

/**
 * This class sends a sos message to the owner
 */
public class SOSMessage
{
    private Context context;
    private PendingIntent sentPI;
    private static final String SENT = "SMS_SENT";

	public SOSMessage(Context _context)
	{
		//new LocationMonitor(_context);
		context = _context;
	}
	
/**
 * This method initialises the activity.
 * It determines the device location, owner number and initiates the sos message to be sent.
 */
	public void findMe(float[] _location)
	{
        sentPI = PendingIntent.getBroadcast(context, 0, new Intent(SENT), 0);

		StringBuilder builder = new StringBuilder();
		if( (ContactTarget.message).length()!=0 )
		{
			builder.append(ContactTarget.message);
			builder.append(' ');
		}
		builder.append(context.getString(R.string.TAG_PLACE));
		
		if( (ContactTarget.place).length() == 0)
		{	builder.append(context.getString(R.string.defaltPlace));}
		else
		{	builder.append(ContactTarget.place);	}
		
		builder.append(' ');
		builder.append(context.getString(R.string.TAG_URL));
		builder.append(' ');
		builder.append(Driver.getIamhereURL());
		builder.append(roundNum(_location[0]));
		builder.append(',');
		builder.append(roundNum(_location[1]));
		
		sendSMS(ContactTarget.number, builder.toString());
	}
	
	private float roundNum(float num)
	{ return Math.round(num * 100000)/100000.0f; }
	
	/**
	 * This method takes a phone number and a text message body and sends it in a sms message
	 * @param phoneNumber the sms recipient
	 * @param message the message body which includes the device gps location
	 */
    private void sendSMS(final String _phoneNumber, final String _message)
    {     
    	//final String num = phoneNumber;
    	//final String message = _message;
    	//Log.v("1234567890",_message);
    	final ProgressDialog dialog = ProgressDialog.show(context, context.getString(R.string.sending), 
    			context.getString(R.string.wait), true);
		final Handler handler = new Handler() 
		{
		   @Override
		   public void handleMessage(Message msg) 
		   {
		      dialog.dismiss();
		      Toast.makeText(context, R.string.sent, Toast.LENGTH_LONG).show();
		   }
	   };
		Thread checkUpdate = new Thread() 
		{  
		   @Override
		   public void run() 
		   {
			   SmsManager sms = SmsManager.getDefault();
		       sms.sendTextMessage(_phoneNumber, null, _message, sentPI, null);
		         
			   //TODO:For debug only
		       //Log.v("iamhere",_phoneNumber+" : "+_message);
		        
		       try
		       {	Thread.sleep(1000*3);	} 
		       catch (InterruptedException e) 
			   {}
				 
		       handler.sendEmptyMessage(0);

		   }
		};
		checkUpdate.start();
    }
}