package com.mmtechco.iamhere;

import com.mmtechco.iamhere.contacts.ContactTarget;
import com.mmtechco.iamhere.contacts.ContactsResource;
import com.mmtechco.iamhere.data.DataKeyValue;
import com.mmtechco.iamhere.data.DataManager;
import com.mmtechco.iamhere.enums.pressTypes;
//import com.mmtechco.iamhere.screens.dialogs.ColorPickerDialog;
//import com.mmtechco.iamhere.screens.dialogs.ColorPickerDialog.OnColorChangedListener;
import com.mmtechco.iamhere.mColorPicker.ColorPickerDialog;
import com.mmtechco.iamhere.mColorPicker.views.ColorPickerView.OnColorChangedListener;
import com.mmtechco.iamhere.screens.MainLayout;
import com.mmtechco.iamhere.screens.SettingsScreen;
import com.mmtechco.iamhere.sms.SOSMessage;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.DialogInterface.OnClickListener;
import android.content.DialogInterface.OnMultiChoiceClickListener;
import android.content.Intent;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.CompoundButton;
import android.widget.CompoundButton.OnCheckedChangeListener;
import android.widget.EditText;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.Toast;

public class ButtonControl implements OnClickListener,OnItemClickListener,OnMultiChoiceClickListener, android.view.View.OnClickListener,OnCheckedChangeListener,OnColorChangedListener
{
	private static Context 			context;
	private static Gui 				display;
	private static ContactsResource contacts;
	private static boolean[] 		multiCheck;
	private static DataManager 		databaseManager;
	private static DataKeyValue 	dkv;
	private pressTypes 				typeOfPress;
	private String[] 				contactNums;
	private AlertDialog 			dialogHandle;
	private Object					param;
	
	private ButtonControl()
	{}
	
	public ButtonControl(Activity _context)
	{
		this(_context, null);
	}
	
	public ButtonControl(Activity _context, pressTypes _typeOfButtonPressed)
	{
		this(_typeOfButtonPressed, null);
		context  = _context;
		if(null == dkv)
		//{dkv 	 = DataKeyValue.getInstance(_context);}
		{dkv 	 = new DataKeyValue(context);}
		display  = Gui.getInstance(_context);
		contacts = ContactsResource.getInstance(_context);
		
		if(null == databaseManager)
		{databaseManager = new DataManager(context);}		
	}
	
	public ButtonControl(pressTypes _typeOfButtonPressed)
	{
		this(_typeOfButtonPressed, null);
	}
	
	public ButtonControl(pressTypes _typeOfButtonPressed, Object _param)
	{
		this();
		param = _param;
		typeOfPress = _typeOfButtonPressed;
	}
	
	private void deletePlace(String placeName)
	 {
		 if (databaseManager.deletePlace(placeName))
		 {
			 Toast.makeText(context, R.string.placeDeleted, Toast.LENGTH_SHORT).show();
			
			 if(0==databaseManager.getAllPlaces().length)
			 {	 display.switchToMyLocationMode();	 }
			 else
			 {	 display.restoreScreen();		}
		 }
		 else
		 {
			 Toast.makeText(context, R.string.placeNotDeleted, Toast.LENGTH_SHORT).show();
		 }
	}
	
	private void discardNewPlaces()
	{
		new AlertDialog.Builder(context)
        .setMessage(R.string.deleteUnsavedPlaces)
        .setPositiveButton( R.string.yes, new DialogInterface.OnClickListener() 
        {
			@Override
			public void onClick(DialogInterface dialog, int which) 
           { databaseManager.deleteUnsavedPlaces();	  }
        })
        .setNegativeButton( R.string.no, new DialogInterface.OnClickListener() 
        {
			@Override
			public void onClick(DialogInterface dialog, int which) 
           { display.showNewPlaces();	  }	// needed to show the list of new places again if user cancels deletion
        })
        .show();
		
	}
	 /**
	 * Forward a selected place
	 * @param which The numeric id of the selected place
	 */
	private void forwardPlace(int which)
	{
		ContactTarget.place = databaseManager.getAllPlaces()[which];
		ContactTarget.loc   = databaseManager.getLocForPlaceName(ContactTarget.place);
		display.switchToForwardMode(ContactTarget.place);
		//display.switchToForwardMode();
		
		dialogHandle.dismiss();
	}
	

	private void cancelOnGPS()
	{
		//Log.i("iamhere","cancelOnGPS");
		 //display.removeWaitingForGPS();	
		 display.finalize();
	}
	
	 public void onBackPressed()
	 {
		 if(display.isLocationMode())		
		 {
			 new AlertDialog.Builder(context)
	         	.setMessage(R.string.exit)
	         	.setPositiveButton( R.string.yes, new DialogInterface.OnClickListener() 
	         {
				@Override
				public void onClick(DialogInterface dialog, int which) 
	            { display.finalize();  }
	         })
	         .setNegativeButton( R.string.no, null)
	         .show();
		 }
		 else if(display.isManageMode() || display.isForwardMode() || display.isSettingsMode())
		 {
			 display.switchToMyLocationMode();
		 }
		 else
		 {	
			 display.finalize();
		 }
	 }

	 @Override
	public void onClick(DialogInterface dialog, int which) 
	{
		onClick(dialog, which, false);
	}
	 
	/**
	  * This is needed to keep track of which items are checked in the list of "New Places"
	  * Everytime one of them is ticked/unticked, this method will be called.
	  * This method then sets the element in the array to match the items "checked" status.
	  * Be sure to set the size of the array before using this by calling <b>setMultiCheckSize(int)</b>
	  */
	@Override
	public void onClick(DialogInterface dialog, int which, boolean isChecked)
	{		
		switch(typeOfPress)
		{
			case SAVE_THIS_PLACE:
				 saveThisPlace();
				 break;
			case SEND:
				 sendMessage(dialog,which);
				 break;
			case CANCEL_GPS_YES_NO:
				 cancelOnGPS();
				 break;
			case PICK_NUM:
				 pickPhoneNumber(dialog,which);
				 break;
			case PICK_PLACE:
				 placeChecked(which,isChecked);
				 break;
			case FORWARD_PLACE:
				 forwardPlace(which);
				 break;
			case SAVE_PLACES:
				 saveNewPlaces();
				 break;
			case DISCARD_PLACES:
				 discardNewPlaces();
				 break;
			case DO_DELETE_PLACE_IN_MANAGE:
				 deletePlace((String)param);
				 break;
			case SHARE_ON_SOCIAL:
				 shareOnSocial(param.toString());
				 break;
			case SETTINGS_CLICK_COLOUR_TOP:
				//Log.i("iamhere","btnCtrl::onClick::TOP::"+((ColorPickerDialog)param).getNewColour());
				 onColorChanged(((ColorPickerDialog)param).getNewColour());
				 break;
			case SETTINGS_CLICK_COLOUR_BOTTOM:
				//Log.i("iamhere","btnCtrl::onClick::TOP::"+((ColorPickerDialog)param).getNewColour());
				 onColorChanged(((ColorPickerDialog)param).getNewColour());
				 break;
			case SETTINGS_CLICK_COLOUR_FONT:
				//Log.i("iamhere","btnCtrl::onClick::TOP::"+((ColorPickerDialog)param).getNewColour());
				 onColorChanged(((ColorPickerDialog)param).getNewColour());
				 break;
			case CANCEL_IN_COLORPICKER:
				//Log.i("iamhere","btnCtrl::onClick::CANCEL");
				 break;
		}
	}

	//OnClickListener for buttons in "Manage Places" view
	@Override
	public void onClick(View v)
	{
		switch(typeOfPress)
		{
		case VIEW_MAPURL_IN_BROWSER:		// The data in "Tag" is set in the calling method
			 //THIS LAUNCHES THE APP PICKER
			 //display.showViewURLOptions((String)param);  //param contains URL of coords
			
			 //THIS LAUNCHES THE BROWSER ONLY
			 display.showURLinBrowser((String)param);  //param contains URL of coords
			 
			 break;
		case CLICK_GEO_APP_IN_MANAGE:
			 //start intent in param
			 //v.postInvalidate();
			 display.startGeoIntent((Intent) param, (String)v.getTag());
			 break;
		case ASK_DELETE_PLACE_IN_MANAGE:
			 display.showDeleteConfirmation(String.valueOf(v.getTag())); // Tag contains Name of place to "X"
			 break;
		case SAVE_CHANGES_IN_MANAGE:
			 saveChangesInManage();			
			 break;
		case SHOW_ABOUT_IN_SETTINGS:
			 showAbout();
			 break;
		case SETTINGS_CLICK_COLOUR_TOP:
		case SETTINGS_CLICK_COLOUR_BOTTOM:
		case SETTINGS_CLICK_COLOUR_FONT:
			 try {
				display.clickColour((int[])param,typeOfPress);
			} catch (IllegalAccessException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			 break;		 
		}		
	}
	
	private void shareOnSocial(String tweet)
	{
		display.shareOnSocial(tweet);
	}
	
	public boolean onCreateOptionsMenu(Menu menu)
	 {
	     return display.showFooterMenu(menu);
	 }
	
	@Override
	public void onItemClick(AdapterView<?> arg0, View arg1, int position, long arg3) 
	{
		switch(typeOfPress)
		{
			case PICK_CONTACT:
				display.showSendDialog(contacts.getNumbers(position),contacts.getName(position));
				break;
		}
	}

	public boolean onOptionsItemSelected(MenuItem item)
	 {
		 boolean outcome = false; 
		 
	     // Handle item selection
	     switch (item.getItemId()) 
	     {
	     case R.id.saveThisPlaces:
	          display.saveCurrentPlace();
	    	  outcome = true;
	    	  break;
	     case R.id.shareplaces:
	          display.showForwardPlaces();
	    	  outcome = true;
	    	  break;
	   /*case R.id.homeplace:
	    	  display.switchToMyLocationMode();
	    	  outcome = true;
	    	  break;
	     case R.id.about:
	          showAbout();
	          outcome = true;
	          break;*/
	     case R.id.manageplaces:
	          display.switchToManagePlaces();
	          outcome = true;
	          break;
	     case R.id.settings:
	    	  display.showSettings();
	    	  outcome=true;
	    	  break;
	     }
	     return outcome;
	 }
	
	public void showAbout()
	{
		display.showAbout();
	}
	
	public boolean onPrepareOptionsMenu(Menu menu)
	{
		return display.refreshFooterMenu(menu);
	}

	private void pickPhoneNumber(DialogInterface dialog, int which)
	{
		ContactTarget.number = contactNums[which]; 
	}

	private void placeChecked(int which, boolean isChecked)
	{
		multiCheck[which]=isChecked;		
	}
	
	/**
	  * Populate a string[] array with the string values taken from the edit boxes
	  * in the Manage Places page, and then forward them on to Gui.saveEditedPlaces(String[])
	  */
	private void saveChangesInManage()
	{
		 EditText[] placeBoxes = (EditText[])param;
		 String[] places = new String[placeBoxes.length];
		 for(int count = 0; count<places.length;count++)
		 {places[count] = placeBoxes[count].getText().toString();}
		 display.saveEditedPlace(places); // Tag contains array[] of places to compare with original		
	}

	/**
	 * Used in multi check lists only.
	 * Will retrieve a list of all new places from the database, and only add the ones that were ticked
	 * by the user.
	 */
	private void saveNewPlaces()
	{
		//SAVE
		String[] newPlaces = databaseManager.getNewPlaces();

		//Add place names to database one by one...
		for(int count=0 ; count < multiCheck.length; count++)
		{	//...but only if they've been selected (ie, element = true)
			if (multiCheck[count])
			{	databaseManager.saveNewPlaces(newPlaces[count]);   }
		}
		
		//Now Discard
		databaseManager.deleteUnsavedPlaces();
		//((Dialog) param).dismiss();
		display.dismissNewPlaces();
	}
		
	 private void saveThisPlace()
	{	
		int rId = 0;
		String placeName = display.getPlaceName().trim();
		if(placeName.equals(""))
		{
			rId = R.string.noPlaceNameToSave;
		}
		else if(databaseManager.placeExists(placeName))
		{
			display.saveCurrentPlace();
			rId = R.string.nameInUse;
		}
		else
		{
			float[] loc =  LocationMonitor.getinstance(context).getGPSNow();
			databaseManager.addEntry(loc[0],loc[1],placeName,false);
			rId = R.string.savedPlace;
			ContactTarget.clear();
		}
		
		Toast.makeText(context, rId, Toast.LENGTH_SHORT).show();
	}	 
	 
	 private void sendMessage(DialogInterface dialog, int which)
	{
		SOSMessage message = new SOSMessage(context);
    	if(ContactTarget.number.equals(""))
    	{ContactTarget.number = contactNums[0];}

		if(0.0f == ContactTarget.loc[0] 
		&& 0.0f == ContactTarget.loc[1])
 		{
 			ContactTarget.loc=LocationMonitor.getinstance(context).getGPSNow();
 		}
    	    	
    	ContactTarget.place   = display.getPlaceName();
    	ContactTarget.message = display.getMessage();
    	if(ContactTarget.place.length() <= 0)
    	{ContactTarget.place=context.getString(R.string.defaltPlace);}
    	else//save new place to db
    	{	databaseManager.addEntry(ContactTarget.loc[0], ContactTarget.loc[1], ContactTarget.place,false);
    	}
    	
    	message.findMe(ContactTarget.loc);
    	//Show social screen
    	if(dkv.getValue(SettingsScreen.SHOW_SHARE_BOX).equalsIgnoreCase(DataKeyValue.TRUE)?true:false)
    	{
    		display.buildSocialScreen();
    	}
	}

	public void setDialogHandler(AlertDialog ad)
	{
		dialogHandle = ad;
	}

	 /**
	  * Sets the size of the array containing the status (true/false) of each check box.
	  * @param size The number of new places that were found in the database
	  */
	 public void setMultiCheckSize(int size)
	 {
		 multiCheck = new boolean[size];
	 }
	 
	public void setNumbersInUse(String[] _numbers)
	{
		contactNums = _numbers;
	}

	@Override
	public void onCheckedChanged(CompoundButton buttonView, boolean isChecked)
	{
		//Log.i("iamhere","Box is"+(isChecked?" ":" not ")+"checked");
		dkv.setValue(SettingsScreen.SHOW_SHARE_BOX, isChecked ? DataKeyValue.TRUE
															  : DataKeyValue.FALSE );
	}
/*
	@Override
	public void colorChanged(int color)
	{
		switch(typeOfPress)
		{
			case SETTINGS_CLICK_COLOUR_TOP:
				dkv.setValue(MainLayout.COLOUR_TOP,color+"");
				 break;
			case SETTINGS_CLICK_COLOUR_BOTTOM:
				dkv.setValue(MainLayout.COLOUR_BOTTOM,color+"");
				 break;
			case SETTINGS_CLICK_COLOUR_FONT:
				dkv.setValue(MainLayout.COLOUR_TEXT,color+"");
				 break;
		}
		
		if(display.isSettingsMode())
		{display.showSettings();}
	}*/

	@Override
	public void onColorChanged(int color)
	{
		//Log.i("iamhere","in onColorChanged::ButtonControl::"+typeOfPress.toString()+" and colour="+color);
		
		switch(typeOfPress)
		{
			case SETTINGS_CLICK_COLOUR_TOP:
				 dkv.setValue(MainLayout.COLOUR_TOP,color+"");
				 break;
			case SETTINGS_CLICK_COLOUR_BOTTOM:
				 dkv.setValue(MainLayout.COLOUR_BOTTOM,color+"");
				 break;
			case SETTINGS_CLICK_COLOUR_FONT:
				 dkv.setValue(MainLayout.COLOUR_TEXT,color+"");
				 break;
		}
		
		if(display.isSettingsMode())
		{display.showSettings();}		
	}
}