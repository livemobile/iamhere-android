package com.mmtechco.iamhere.enums;

public enum ScreenType {
	LOCATION, 
	FORWARD, 
	MANAGE, 
	SETTINGS;
}
