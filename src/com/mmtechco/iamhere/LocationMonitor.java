package com.mmtechco.iamhere;

import java.util.Date;
import java.util.List;

import com.mmtechco.iamhere.interfaces.CallBack;

import android.content.Context;
import android.location.Criteria;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.Bundle;

/**
 * 
 * Monitors and records the GPS location on the device at specific intervals.
 */
public class LocationMonitor
{
	private static LocationMonitor privateLoc = null;
	
	private LocationManager loma;
	//private Location 		location;
	private Criteria 		criteria;
	private float 			resultArray[];
	private LocationListener locatLister;
	private final int oldTimeLimit = 300000;
/**
 * Sets the local storage location and the interface to the global environment that the current application is running in.
 * 
 * @param _context Interface to global environment that the current application is running in.
 * @param _localData Local storage location
 */
	public static LocationMonitor getinstance(Context _context)
	{
		if(null == privateLoc)
		{privateLoc = new LocationMonitor(_context);}
		
		return privateLoc;
	}
	
	private LocationMonitor(){}
	private LocationMonitor(Context _context)
	{
		loma = (LocationManager)_context.getSystemService(Context.LOCATION_SERVICE);
		resultArray = new float[]{0,0};
		
	    criteria = new Criteria(); 
	    criteria.setAltitudeRequired(false); 
	    criteria.setBearingRequired(false); 
	    criteria.setCostAllowed(true);
	}
	
	public void requestUpdates(final CallBack _callBack)
	{
		locatLister = new LocationListener()
		{	@Override
			public void onLocationChanged(Location loc)
			{	
				_callBack.buildOutContacts(loc);
			}

			@Override
			public void onProviderDisabled(String provider) {}
			@Override
			public void onProviderEnabled(String provider) {}
			@Override
			public void onStatusChanged(String provider, int status,Bundle extras) {}
		};
		
		List<String> listProviders = loma.getAllProviders();
		for(String provider : listProviders)
		{loma.requestLocationUpdates(provider, 1000, 2, locatLister);}
	}
	
	public void stopUpdates()
	{
		if(null != loma && null != locatLister)
		{
			loma.removeUpdates(locatLister);
		}
	}
	
	public void setGPS(float lat, float lon)
	{
		resultArray[0] = lat;
		resultArray[1] = lon;
	}
	
	public float[] getGPSNow()
	{
		String provider = loma.getBestProvider(criteria, true);
		long currentTime = new Date().getTime();
		float[] resultArrayTemp = null;
		
		if(null != provider)
		{
			resultArrayTemp = latLonFromProvider(provider,currentTime);
			if(null != resultArrayTemp)
			{
				resultArray = resultArrayTemp; 
			}
		}
		
		//no result for best provider was found
		if(null == resultArrayTemp)
		{
			List<String> listProviders = loma.getAllProviders();
			for(String provid : listProviders)
			{
				resultArrayTemp = latLonFromProvider(provid,currentTime);
				if(null != resultArrayTemp)
				{	resultArray = resultArrayTemp; 	break;	}
			}
		}
		return resultArray;
	}
	
	private float[] latLonFromProvider(String provider, long currentTime)
	{
		float[] resultArray = null;

		Location location = loma.getLastKnownLocation(provider);
		
		if(null != location 
		&& location.getTime()+oldTimeLimit > currentTime)
		{
			resultArray = new float[]{
							(float)location.getLatitude(),
							(float)location.getLongitude()};
		}
		
		return resultArray;
	}
}