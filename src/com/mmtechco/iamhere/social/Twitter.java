package com.mmtechco.iamhere.social;

import java.util.List;

import com.mmtechco.iamhere.R;

import android.app.Activity;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.content.pm.ResolveInfo;

public class Twitter
{
	//private String   twitterURL = "twitter://twitter.com/intent/tweet?text=";
	private Activity activity;
	
	private Twitter(){}	
	public  Twitter(Activity _activity)
	{
		this();
		activity=_activity;		
	}
	
	public void sendToTwitter(String tweet)
	{
		
		/*
		 final PackageManager pm = activity.getPackageManager();
		//get a list of installed apps.
		        List<ApplicationInfo> packages = pm
		                .getInstalledApplications(PackageManager.GET_META_DATA);

		        for (ApplicationInfo packageInfo : packages)
		        {
		            Log.d("iamhere", "Installed package :" + packageInfo.packageName);
		            Log.d("iamhere", "Launch Activity :"
		                            + pm.getLaunchIntentForPackage(packageInfo.packageName)); 

		        }// the getLaunchIntentForPackage returns an intent that you can use with startActivity() 
		    }*/
		
		Intent tweetIntent = new Intent(Intent.ACTION_SEND);
		tweetIntent.putExtra(Intent.EXTRA_SUBJECT,activity.getString(R.string.shareSubject) );
		tweetIntent.putExtra(Intent.EXTRA_TEXT, tweet);
		//tweetIntent.setType("vnd.android.cursor.item/vnd.twitter.profile");
		//tweetIntent.setType("vnd.android.cursor.item/vnd.twitter.tweet");
		//tweetIntent.setType("com.google.android.C2DMReceiver");
		tweetIntent.setType("text/plain");
		
		PackageManager pm = activity.getPackageManager();
		List<ResolveInfo> lract = pm.queryIntentActivities(tweetIntent,PackageManager.MATCH_DEFAULT_ONLY);

		boolean resolved = false;

		for(ResolveInfo ri: lract)
		{
		    if(ri.activityInfo.name.contains("com.android.twitter"))
		    {
		        tweetIntent.setClassName(ri.activityInfo.packageName,ri.activityInfo.name);
		        resolved = true;
		        break;
		    }
		}
		
		activity.startActivity(resolved ? tweetIntent :
		    							  Intent.createChooser(tweetIntent, null));
	}
}