package com.mmtechco.iamhere.social;

import java.util.List;

import com.mmtechco.iamhere.ButtonControl;
import com.mmtechco.iamhere.R;
import com.mmtechco.iamhere.contacts.ContactTarget;
import com.mmtechco.iamhere.contacts.ContactsResource;
import com.mmtechco.iamhere.data.DataKeyValue;
import com.mmtechco.iamhere.enums.pressTypes;
import com.mmtechco.iamhere.screens.SettingsScreen;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.AlertDialog.Builder;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.content.pm.ResolveInfo;
import android.view.ContextThemeWrapper;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;

public class Social
{
	//private String   twitterURL = "twitter://twitter.com/intent/tweet?text=";
	private Activity activity;
	
	private Social(){}	
	public  Social(Activity _activity)
	{
		this();
		activity=_activity;		
	}
	
	public void sendToTwitter(String tweet)
	{
		
		/*
		 final PackageManager pm = activity.getPackageManager();
		//get a list of installed apps.
		        List<ApplicationInfo> packages = pm
		                .getInstalledApplications(PackageManager.GET_META_DATA);

		        for (ApplicationInfo packageInfo : packages)
		        {
		            Log.d("iamhere", "Installed package :" + packageInfo.packageName);
		            Log.d("iamhere", "Launch Activity :"
		                            + pm.getLaunchIntentForPackage(packageInfo.packageName)); 

		        }// the getLaunchIntentForPackage returns an intent that you can use with startActivity() 
		    }*/
		
		Intent tweetIntent = new Intent(Intent.ACTION_SEND);
		//tweetIntent.putExtra(Intent.EXTRA_SUBJECT,activity.getString(R.string.shareSubject) );
		tweetIntent.putExtra(Intent.EXTRA_TEXT, tweet);
		//tweetIntent.setType("vnd.android.cursor.item/vnd.twitter.profile");
		//tweetIntent.setType("vnd.android.cursor.item/vnd.twitter.tweet");
		//tweetIntent.setType("com.google.android.C2DMReceiver");
		tweetIntent.setType("text/plain");
		
		PackageManager pm = activity.getPackageManager();
		List<ResolveInfo> lract = pm.queryIntentActivities(tweetIntent,PackageManager.MATCH_DEFAULT_ONLY);

		boolean resolved = false;

		for(ResolveInfo ri: lract)
		{
		    if(ri.activityInfo.name.contains("com.android.twitter"))
		    {
		        tweetIntent.setClassName(ri.activityInfo.packageName,ri.activityInfo.name);
		        resolved = true;
		        break;
		    }
		}
		
		activity.startActivity(resolved ? tweetIntent :
		    							  Intent.createChooser(tweetIntent, null));
	}
	
	/**
	 * Build screen to allow user to share locations on both Twitter and Facebook.
	 */
	public Builder buildSocialScreen(Activity _activity)
	{
		Builder dialog = new AlertDialog.Builder(new ContextThemeWrapper(_activity, android.R.style.Theme_Dialog));

		dialog.setTitle(R.string.tellyourfriends);
		
        LinearLayout socialMediaLayout = new LinearLayout(_activity);
        socialMediaLayout.setOrientation(LinearLayout.VERTICAL);

        //Build text box preview
		StringBuilder tweet = new StringBuilder();
		tweet.append(_activity.getString(R.string.tweetStart));
		ContactsResource contacts = ContactsResource.getInstance(_activity);
		tweet.append(' ');
		tweet.append(contacts.getContactNameFromNumber(ContactTarget.number));
		tweet.append(' ');
		tweet.append(_activity.getString(R.string.tweetMiddle));
		tweet.append(' ');
		tweet.append(ContactTarget.place);
		tweet.append(' ');
		tweet.append(_activity.getString(R.string.tweetEnd));
		
        EditText textPreview = new EditText(_activity);
        textPreview.setMinLines(3);
        textPreview.setText(tweet);
        textPreview.setText(textPreview.getText());
        
        /*
        View twitRuler = new View(activity); 
        twitRuler.setBackgroundColor(0xFFFFFFFF);
        socialMediaLayout.addView(twitRuler, new ViewGroup.LayoutParams( ViewGroup.LayoutParams.FILL_PARENT, 2));
        
        //socialMediaLayout.addView(fbImage);
        View fbRuler = new View(activity); 
        fbRuler.setBackgroundColor(0xFFFFFFFF);
        socialMediaLayout.addView(fbRuler, new ViewGroup.LayoutParams( ViewGroup.LayoutParams.FILL_PARENT, 2));
        *
        */
        socialMediaLayout.addView(textPreview);
        
        //Show tick box at the bottom to show/hide screen
        LinearLayout showHideTickBoxView = new LinearLayout(_activity);
        CheckBox showHideCheckBox = new CheckBox(_activity);
        showHideCheckBox.setOnCheckedChangeListener(new ButtonControl(pressTypes.SHOW_HIDE_CHECKED));
        showHideCheckBox.setChecked(new DataKeyValue(_activity).getValue(SettingsScreen.SHOW_SHARE_BOX).equalsIgnoreCase(DataKeyValue.TRUE)?true:false);
        
        TextView showHideTextView = new TextView(_activity);
        showHideTextView.setText(R.string.showHideInShareBox);
        
        showHideTickBoxView.addView(showHideCheckBox);
        showHideTickBoxView.addView(showHideTextView);

        socialMediaLayout.addView(showHideTickBoxView);
        
        //Set positive/negative buttons
        dialog.setPositiveButton(R.string.share, new ButtonControl(pressTypes.SHARE_ON_SOCIAL,textPreview.getText()));
        dialog.setNegativeButton(R.string.cancel, null);
        dialog.setView(socialMediaLayout);  
        //dialog.show();
        
        return dialog;
	}
}