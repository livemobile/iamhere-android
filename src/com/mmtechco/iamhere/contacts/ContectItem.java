package com.mmtechco.iamhere.contacts;

import java.util.ArrayList;

public class ContectItem 
{
	private ArrayList<String> numbers;
	private String name;

	public ContectItem(String _name)
	{
		this();
		name = _name;
	}
	
	private ContectItem()
	{
		numbers = new ArrayList<String>();
	}
	
	public void addNumber(String _num)
	{
		String inputtedNumber = _num.replaceAll("[-\\s.\\+]", "");
		for(String num :numbers)
		{
			if(inputtedNumber.equals(num))
			{return;}
		}
		numbers.add(inputtedNumber);
	}
	
	@Override
	public String toString()
	{
		return name;
	}
	
	public String[] getNumber()
	{
		return numbers.toArray(new String[numbers.size()]);
	}
}
