package com.mmtechco.iamhere.contacts;

import java.util.ArrayList;


import android.content.Context;
import android.database.Cursor;
import android.net.Uri;
import android.provider.ContactsContract.Contacts;
import android.provider.ContactsContract.CommonDataKinds.Phone;
import android.provider.ContactsContract.PhoneLookup;

public class ContactsResource 
{
	 private String[]   contactNames;
	 private String[][] contactNums;
	 private Context	context;
	 
	 private static ContactsResource contacts;
	 
	 public static ContactsResource getInstance(Context _activity)
	 {
		if(null==contacts)
		{
			contacts = new ContactsResource(_activity);
		}
		
		return contacts;
	 }
	 
	 public static ContactsResource getInstance(Context _context,boolean populateInfo)
	 {
		if(null==contacts)
		{
			contacts = new ContactsResource(_context,populateInfo);
		}
		
		return contacts;
	 }	 
	 
	 private ContactsResource()
	 {
	 }
	 
	 private ContactsResource(Context _context)
	 { 
		 this(_context, true);
	 }
	 
	 private ContactsResource(Context _context, boolean populateContactInfo)
	 { 	 
		 this();
		 context = _context;
		 if(populateContactInfo)
		 {queryContacts();}
	 }
	 
	public String getContactNameFromNumber(String inputNumber) 
	{
		String name = "";
		Uri uri = Uri.withAppendedPath(PhoneLookup.CONTENT_FILTER_URI, Uri.encode(inputNumber));
		Cursor cur = context.getContentResolver().query(uri, new String[]{PhoneLookup.DISPLAY_NAME}, null,null, null);
		
		if (cur.moveToFirst()) 
		{
			name = cur.getString(cur.getColumnIndex(PhoneLookup.DISPLAY_NAME));
		}
		cur.close();
		// return the original number if no match was found
		return name;
	}
	 
	 private void queryContacts()
	 {
		 String[] PROJECTION=new String[] 
		{ Contacts.DISPLAY_NAME, Phone.NUMBER};
   		 
   		 Cursor people = context.getContentResolver().query(Phone.CONTENT_URI, PROJECTION, null, null,  PROJECTION[0]+ " ASC");
   		 int nameFieldColumnIndex   = people.getColumnIndex(Contacts.DISPLAY_NAME);
   		 int numberFieldColumnIndex = people.getColumnIndex(Phone.NUMBER);
   		 
   		 ArrayList<ContectItem> contactList = new ArrayList<ContectItem>();
   		 ContectItem tempItem = null;
   		 
   		 while(people.moveToNext()) 
   		 {
   			 String personName = people.getString(nameFieldColumnIndex);
   			 
   			 if(null == tempItem 
   			|| false == personName.equals(tempItem.toString()))
   			 {
   				 if(null != tempItem)
   				 {	contactList.add(tempItem);	}
   				 tempItem = new ContectItem(personName);
   			 }
   			 
   			 tempItem.addNumber(people.getString(numberFieldColumnIndex));
   		 }

   		 people.close();
   		 
   		 final int contactListSize = contactList.size();
   		 contactNames = new String[contactListSize];
   		 contactNums  = new String[contactListSize][];
   		 
   		 for(int index = 0; contactListSize > index; index++)
   		 {
   			 tempItem = contactList.get(index);
   			 contactNames[index] = tempItem.toString();
   			 contactNums [index] = tempItem.getNumber();
   		 }
	 }
	 
	 public String[] getNames()
	 {
		 return contactNames;
	 }
	 
	 public String getName(int position)
	 {
		 return contactNames[position];
	 }
	 
	 public String[] getNumbers(int position)
	 {
		 return contactNums[position];
	 }
}