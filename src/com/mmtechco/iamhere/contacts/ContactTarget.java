package com.mmtechco.iamhere.contacts;

public class ContactTarget 
{
	public static String  number;
	public static String  place;
	public static String  message;
	public static float[] loc;
	
	private ContactTarget()
	{}
	
	public static void clear()
	{
		number  = "";
		place   = "";
		message = "";
		loc		= new float[]{0.0f,0.0f};
	}
}