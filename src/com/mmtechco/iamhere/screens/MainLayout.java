package com.mmtechco.iamhere.screens;

import com.mmtechco.iamhere.data.DataKeyValue;

import android.content.Context;
import android.graphics.Color;
import android.graphics.drawable.GradientDrawable;
import android.widget.LinearLayout;
import android.widget.TextView;

public class MainLayout extends LinearLayout
{
	public final static String COLOUR_TEXT 	 = "COLOUR_TEXT";
	public final static String COLOUR_TOP 	 = "COLOUR_TOP";
	public final static String COLOUR_BOTTOM = "COLOUR_BOTTOM";
	//private static DataKeyValue vals;
	private DataKeyValue vals;
	private Context context;
	
	public MainLayout(Context _context) 
	{
		super(_context);
		context = _context;
		/*if(null == vals)
		{
			vals = DataKeyValue.getInstance(_context);//new DataKeyValue(context);
		}*/
		vals = new DataKeyValue(_context);
		//Log.v("123456789",Locale.getDefault().getCountry());
		/*if(Driver.getZone() == Region.CH)//if(Locale.getDefault().getCountry().equalsIgnoreCase("CN"))
		{setBackgroundResource(R.drawable.gradient_sky);}
		else
		{*/
		  GradientDrawable gradient = new GradientDrawable(
		            GradientDrawable.Orientation.TOP_BOTTOM,
		            new int[] {vals.getValueAsINT(COLOUR_TOP),
		            		   vals.getValueAsINT(COLOUR_BOTTOM)});
		  gradient.setCornerRadius(0f);

		  setBackgroundDrawable(gradient);//}
		
	}
	
	public int getTextColour()
	{
		return vals.getValueAsINT(COLOUR_TEXT);
	}
	
	public int getTitleTextColor()
	{
		int topColor = vals.getValueAsINT(COLOUR_TOP);
		if (Color.red(topColor)+Color.green(topColor)+Color.blue(topColor) < 384)
		{	return Color.WHITE;}
		else
		{	return Color.BLACK;}
	}
	
	public void setTextColour(String colour)
	{
		vals.setValue(COLOUR_TEXT, colour);
	}
	
	public int[] getBgColour()
	{
		return new int[]{vals.getValueAsINT(COLOUR_TOP),vals.getValueAsINT(COLOUR_BOTTOM)};
	}
	
	public void setBgTopColour(int topColour)
	{
		vals.setValue(COLOUR_TOP, topColour+"");
	}	
	
	public void setBgBottomColour(int bottomColour)
	{
		vals.setValue(COLOUR_BOTTOM, bottomColour+"");
	}
	
	public int getSeparatorColour()
	{
		int bottomColor = vals.getValueAsINT(COLOUR_BOTTOM);
		if (Color.red(bottomColor)+Color.green(bottomColor)+Color.blue(bottomColor) < 384)
		{	return Color.WHITE;}
		else
		{	return Color.BLACK;}
	}
	
	public TextView getNewTextView(int lableText)
	{
		TextView colourTop = new TextView(context);
		colourTop.setTextColor(getTextColour());
		colourTop.setText(lableText);
		colourTop.setTextSize(20);
		
		return colourTop;
	}
}
