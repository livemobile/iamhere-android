package com.mmtechco.iamhere.screens;

import com.mmtechco.iamhere.enums.Directions;

import android.view.ViewGroup;
import android.view.animation.AlphaAnimation;
import android.view.animation.Animation;
import android.view.animation.AnimationSet;
import android.view.animation.LayoutAnimationController;
import android.view.animation.TranslateAnimation;

public class UIHelper 
{

    /**
     * Set direction that children in the panel will slide in from when next
     * displayed.
     * 
     * @param panel
     *            {@link ViewGroup} whose children will be slid in from the
     *            specified direction when the panel is next displayed.
     * @param fromDirection
     *            Primitive int indicating the direction to slide the children
     *            of the panel from.
     */
    public static void setSlideDirection(ViewGroup panel, Directions fromDirection) 
    {

        AnimationSet set = new AnimationSet(true);

        Animation animation = new AlphaAnimation(0.0f, 1.0f);
        animation.setDuration(100);
        set.addAnimation(animation);

        float fromX = 0.0f;
        float fromY = 0.0f;
        float toX 	= 0.0f;
        float toY 	= 0.0f;
        
        switch (fromDirection) 
        {
        case TOP:
            fromY 	=-1.0f;
            break;
        case BOTTOM:
            fromY 	= 1.0f;
            break;
        case LEFT:
            fromX 	=-1.0f;
            break;
        case RIGHT:
        default:
            fromX 	= 1.00f;
            break;
        }

        animation = new TranslateAnimation(Animation.RELATIVE_TO_SELF, fromX,
						Animation.RELATIVE_TO_SELF, toX, Animation.RELATIVE_TO_SELF,
						fromY, Animation.RELATIVE_TO_SELF, toY);
        animation.setDuration(200);
        set.addAnimation(animation);

        LayoutAnimationController controller = new LayoutAnimationController(set, 0.25f);
        panel.setLayoutAnimation(controller);
    }
}