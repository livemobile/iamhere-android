package com.mmtechco.iamhere.screens;

import android.app.Activity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

public class ColoredTextAdapter extends ArrayAdapter<String> {
    private Activity context;
    private String[] items;
    private int colour;
    
    public ColoredTextAdapter(Activity aContext, String[] items, int _colour) 
    {
            super(aContext, android.R.layout.simple_list_item_1, items);
            context = aContext;
            this.items = items;
            colour = _colour;
    }

    @Override
	public View getView(int position, View convertView, ViewGroup parent) 
    {

            View row = convertView;
            if (row == null) 
            {
                    LayoutInflater inflater = context.getLayoutInflater();
                    row = inflater.inflate(android.R.layout.simple_list_item_1, null);
                   // row.setBackgroundColor(Color.BLUE);
            }
            TextView text = (TextView)row.findViewById(android.R.id.text1);
            text.setTextColor(colour);
            text.setText(items[position]);

            return row;
    }
}