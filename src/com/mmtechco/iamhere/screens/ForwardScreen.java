package com.mmtechco.iamhere.screens;

import com.mmtechco.iamhere.ButtonControl;
import com.mmtechco.iamhere.Gui;
import com.mmtechco.iamhere.R;
import com.mmtechco.iamhere.contacts.ContactTarget;
import com.mmtechco.iamhere.enums.Directions;
import com.mmtechco.iamhere.enums.ScreenType;
import com.mmtechco.iamhere.enums.pressTypes;

import android.app.Activity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.LinearLayout;

public class ForwardScreen extends Activity
{
	private CustomTitle   customTitle;
	private String		  headerString ;
	private int			  imgID;
	private MainLayout 	  mainLayout;
	private ButtonControl buttonControl;
	
	@Override
	public void onCreate(Bundle savedInstanceState) 
	{
		super.onCreate(savedInstanceState);
		//activity   = this;
		getWindow().setWindowAnimations(android.R.style.Animation);
		buttonControl = new ButtonControl(this, pressTypes.WINDOW);
		mainLayout	  = new MainLayout(this);
		mainLayout.setOrientation(LinearLayout.VERTICAL);
		//mainLayout.setBackgroundResource(R.drawable.gradient);

		setHeader();
		buildLayout();
		UIHelper.setSlideDirection(mainLayout, Directions.RIGHT);
		setContentView(mainLayout);
	}
	
	private void buildLayout()
	{
		mainLayout.addView(customTitle.getView());
		mainLayout.addView(ContactList.getInstance(this,mainLayout.getTextColour()).buildList());
		Gui.setMode(ScreenType.FORWARD);//(false,true,false,false);
	}
	
	public LinearLayout getView()
	{
		return mainLayout;
	}
	
	@Override
	 public void onBackPressed()
	 {
		//finishActivity(0);
		LocationScreen.slideLeft(true);
		buttonControl.onBackPressed();
	 }
	
	@Override
	 public boolean onCreateOptionsMenu(Menu menu)
	 {
		 return buttonControl.onCreateOptionsMenu(menu);
	 }	
	
	@Override
	 public boolean onOptionsItemSelected(MenuItem item)
	 {
		 return buttonControl.onOptionsItemSelected(item);
	 }
	 
	 @Override
	 public boolean onPrepareOptionsMenu(Menu menu)
	 {
		return buttonControl.onPrepareOptionsMenu(menu);		 
	 }
	 
	 public void removeAllViews()
	{
		if(null!=mainLayout)
		{mainLayout.removeAllViews();}
	}
	 
	 private void setHeader()
	{
		headerString = getString(R.string.forwardPlaceHead)+ContactTarget.place;
		imgID		 = R.drawable.forwardplacesmall;
		customTitle  = CustomTitle.getInstance(this);
		customTitle.buildHeader(headerString,imgID,mainLayout.getTitleTextColor());		
	}
}