package com.mmtechco.iamhere.screens;

import com.mmtechco.iamhere.ButtonControl;
import com.mmtechco.iamhere.contacts.ContactTarget;
import com.mmtechco.iamhere.contacts.ContactsResource;
import com.mmtechco.iamhere.enums.pressTypes;

import android.app.Activity;
import android.graphics.Color;
import android.widget.ListView;

public class ContactList
{
	private ContactsResource contacts;
	private Activity activity;
	private int textColour;
	//private String[] contactNames;
	
	private static ContactList contactList;
	
	public static ContactList getInstance(Activity _activity, int _textColour)
	{
		if(null==contactList)
		{
			contactList = new ContactList(_activity);
		}
		contactList.textColour = _textColour;
		return contactList;
	}
	
	private ContactList(){}
	
	private ContactList(Activity _activity)
	{
		this();
		activity	= _activity;
		ContactTarget.clear(); //TODO: is this needed? Possibly fixes NULL POINTER error
		contacts	= ContactsResource.getInstance(activity);
	}
	
	public ListView buildList()
	{
		ListView list = new ListView(activity){};
		list.setOnItemClickListener(new ButtonControl(pressTypes.PICK_CONTACT));
		 
		list.setAdapter(new ColoredTextAdapter(activity, contacts.getNames(),textColour));
		list.setScrollingCacheEnabled(false);
		list.setBackgroundColor(Color.TRANSPARENT); 
		//Add fast thumb scroller down the side
		list.setFastScrollEnabled(true);
		list.setSmoothScrollbarEnabled(true);
		
		return list;
	}
}