package com.mmtechco.iamhere.screens;

import com.mmtechco.iamhere.ButtonControl;
import com.mmtechco.iamhere.Gui;
import com.mmtechco.iamhere.R;
import com.mmtechco.iamhere.data.DataManager;
import com.mmtechco.iamhere.enums.Directions;
import com.mmtechco.iamhere.enums.ScreenType;
import com.mmtechco.iamhere.enums.pressTypes;

import android.app.Activity;
import android.graphics.Color;
import android.os.Bundle;
import android.view.Gravity;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.LinearLayout.LayoutParams;
import android.widget.RelativeLayout;
import android.widget.ScrollView;
import android.widget.TextView;
import android.widget.Toast;

public class ManageScreen extends Activity
{
	private CustomTitle 	customTitle;
	private String			headerString ;
	private int				imgID;
	private MainLayout 		mainLayout;
	private LinearLayout    optionsLayout;
	private DataManager		dm;
	private int 			screenWidth;
	private double 			widthOfPlaceTextBox;
	private LayoutParams	lpfw;
	private EditText[]		boxes;
	private String[][] 		myPlaces;
	private ButtonControl	buttonControl;
	
	@Override
	public void onCreate(Bundle savedInstanceState) 
	{
		super.onCreate(savedInstanceState);
		getWindow().setWindowAnimations(android.R.style.Animation);

		buttonControl 	= new ButtonControl(this, pressTypes.WINDOW);//,contacts,display);
		headerString	= getString(R.string.manageplaces_Menu);
		imgID			= R.drawable.managesmall;
		dm				= new DataManager(this);
		myPlaces		= dm.getAllPlacesInfo();
		screenWidth		= getWindowManager().getDefaultDisplay().getWidth();
		widthOfPlaceTextBox = 0.77;		//possibly change this to screenWidth/2
		lpfw = new LayoutParams(android.view.ViewGroup.LayoutParams.FILL_PARENT,android.view.ViewGroup.LayoutParams.WRAP_CONTENT);
		
		mainLayout 		= new MainLayout(this);
		//mainLayout.setBackgroundResource(R.drawable.gradient);
		optionsLayout 	= new LinearLayout(this);
		optionsLayout.setOrientation(LinearLayout.VERTICAL);
			
		setHeader();
		buildBody();
		buildLayout();
		UIHelper.setSlideDirection(mainLayout, Directions.LEFT);
		setContentView(mainLayout);
	}
	
	private void buildBody()
	{
		if(0<myPlaces.length)
		{
			RelativeLayout optionsTitle 	= new RelativeLayout(this);
			RelativeLayout optionsSubTitle	= new RelativeLayout(this);
			LinearLayout   optionsTopBtns 	= new LinearLayout  (this);
			ScrollView     bodyScrollView	= new ScrollView    (this);
			LinearLayout   optionsBody		= new LinearLayout  (this);
		
			optionsTitle = customTitle.getView();
			optionsTitle.setId(101);
			/*RelativeLayout.LayoutParams titleLP = new RelativeLayout.LayoutParams(LayoutParams.WRAP_CONTENT,LayoutParams.WRAP_CONTENT);
			titleLP.addRule(RelativeLayout.ABOVE,optionsTopBtns.getId());
			optionsTitle.setLayoutParams(titleLP);*/
			optionsLayout.addView(optionsTitle);

			//Object to store the place names
			boxes = new EditText[myPlaces.length];			
			//Get the body of the Manage Places screen
			optionsBody=buildManagePlacesBody();
			optionsBody.setOrientation(LinearLayout.VERTICAL);
			optionsBody.setLayoutParams(lpfw);
			optionsBody.setScrollContainer(true);
			optionsBody.setBackgroundColor(0);
			
			//Add it to a scrollable view so it can scroll
			bodyScrollView.setLayoutParams(lpfw);
			bodyScrollView.addView(optionsBody);
			//Build buttons for top of screen
			optionsTopBtns = buildOptionsTitleButtons();
			optionsTopBtns.setId(102);
			RelativeLayout.LayoutParams btnsLP = new RelativeLayout.LayoutParams(android.view.ViewGroup.LayoutParams.WRAP_CONTENT,android.view.ViewGroup.LayoutParams.WRAP_CONTENT);
			btnsLP.addRule(RelativeLayout.ALIGN_PARENT_TOP);
			optionsTopBtns.setLayoutParams(btnsLP);
			optionsLayout.addView(optionsTopBtns);

			//Build banner text above Body
			optionsSubTitle = buildSubTitle();

			optionsLayout.addView(optionsSubTitle);

			RelativeLayout.LayoutParams bodyLP = new RelativeLayout.LayoutParams(android.view.ViewGroup.LayoutParams.WRAP_CONTENT,android.view.ViewGroup.LayoutParams.WRAP_CONTENT);
			bodyLP.addRule(RelativeLayout.BELOW,optionsTitle.getId());
			bodyScrollView.setLayoutParams(bodyLP);
			optionsLayout.setLayoutParams(lpfw);
			optionsLayout.addView(bodyScrollView);	
		}
		else
		{
			Toast.makeText(this, R.string.noplaces, Toast.LENGTH_SHORT).show();
		}
	}
		
	private RelativeLayout buildSubTitle()
	{
		RelativeLayout optionsSubTitle	= new RelativeLayout(this);
		TextView	   txtPlace			= new TextView		(this);
		TextView	   txtView			= new TextView		(this);
		TextView	   txtDelete		= new TextView		(this);
		int textColour = mainLayout.getTextColour();
		
		txtPlace.setId(1);
		txtPlace.setTextColor(textColour);
		txtView.setId(2);
		txtView.setTextColor(textColour);
		txtDelete.setId(3);
		txtDelete.setTextColor(textColour);
		
		txtPlace.setText(R.string.placeName);
		txtView.setText(R.string.view);
		RelativeLayout.LayoutParams lpp = new RelativeLayout.LayoutParams(android.view.ViewGroup.LayoutParams.WRAP_CONTENT,android.view.ViewGroup.LayoutParams.WRAP_CONTENT);
		lpp.addRule(RelativeLayout.LEFT_OF,txtDelete.getId());
		lpp.addRule(RelativeLayout.CENTER_IN_PARENT);
		txtView.setLayoutParams(lpp);
		txtView.setPadding(0, 0, 4, 0);
		
		txtDelete.setText(R.string.delete);
		RelativeLayout.LayoutParams lp = new RelativeLayout.LayoutParams(android.view.ViewGroup.LayoutParams.WRAP_CONTENT,android.view.ViewGroup.LayoutParams.WRAP_CONTENT);
		lp.addRule(RelativeLayout.ALIGN_PARENT_RIGHT);
		lp.addRule(RelativeLayout.CENTER_IN_PARENT);
		txtDelete.setLayoutParams(lp);
		
		optionsSubTitle.addView(txtPlace);
		optionsSubTitle.addView(txtView);
		optionsSubTitle.addView(txtDelete);
		return optionsSubTitle;
	}

	private void buildLayout()
	{
		mainLayout.addView(optionsLayout);
		Gui.setMode(ScreenType.MANAGE);//Gui.setMode(false,false,true,false);
	}
	private LinearLayout buildManagePlacesBody()
	{
		LinearLayout   optionsBody		= new LinearLayout(this);
		RelativeLayout optionsBodySub 	= new RelativeLayout(this);

		//IMPORTANT: Do this or else all views (except one) will be forced off the side of the screen!
		optionsBody.setOrientation(LinearLayout.VERTICAL);
		
		EditText editPlace;
		ImageButton deleteImage = null;
		
		//Build body
		for(int count=0 ; count<myPlaces.length ; count++)
		{
			//Initialise new views for use in each loop
			optionsBodySub = new RelativeLayout(this);
			optionsBodySub.setLayoutParams(lpfw);
			optionsBodySub.setBackgroundColor(Color.TRANSPARENT);

			boxes[count] = new EditText(this);
			editPlace 	 = boxes[count];	 	
			editPlace.setId(4);
			//editPlace.setSelected(false);
			
			//Set up each places text view
			editPlace.setText(myPlaces[count][0]);
			//Set Width based on pre-configured screen width of the device				
			editPlace.setWidth( (int)(screenWidth*widthOfPlaceTextBox) );
			optionsBodySub.addView(editPlace);

			//Now do the delete image (this is done BEFORE browserImage so the ID is set)
			deleteImage=getDeleteImage(myPlaces[count][0]);
			
			//Now do the browser image
			optionsBodySub.addView( getBrowserImage(myPlaces[count][1],myPlaces[count][2],deleteImage.getId()) );
			optionsBodySub.addView(deleteImage);				
						
			//And now to main body layout
			optionsBody.addView(optionsBodySub);
		}
		return optionsBody;
	}
	
	private LinearLayout buildOptionsTitleButtons()
	{
		LinearLayout optionsTopBtns = new LinearLayout(this);

		Button saveButton = new Button(this);
		//Button backButton = new Button(activity);
		
		saveButton.setText(R.string.saveChanges);
		saveButton.setOnClickListener(new ButtonControl(pressTypes.SAVE_CHANGES_IN_MANAGE,boxes));
		saveButton.setWidth(screenWidth/2);	
		
		//backButton.setText(R.string.back);
		//backButton.setOnClickListener(new ButtonControl(pressTypes.CANCEL_IN_MANAGE));
		//backButton.setWidth(screenWidth/2);	
		
		optionsTopBtns.setGravity(Gravity.CENTER);
		optionsTopBtns.setLayoutParams(lpfw);
		optionsTopBtns.addView(saveButton);
		//optionsTopBtns.addView(backButton);
		
		return optionsTopBtns;
	}
		
	/**
	 * Returns an image button view that is built with the given parameters
	 * @param lat The locations latitude
	 * @param lon The locations longitude
	 * @param idOfOtherView The ID of the view this image will be positioned beside
	 * @return Returns ImageButton configured with the provided parameters
	 */
	private ImageButton getBrowserImage(String lat, String lon, int idOfOtherView)
	{
		String linkURL = lat+","+lon;
		ImageButton browserImage = new ImageButton(this);
		ButtonControl buttonMap = new ButtonControl(pressTypes.VIEW_MAPURL_IN_BROWSER,linkURL);
		browserImage.setId(5);			
		browserImage.setImageResource(R.drawable.browserglobe);
		browserImage.setBackgroundColor(Color.TRANSPARENT);
		browserImage.setOnClickListener(buttonMap);
		//browserImage.setTag(linkURL);// send URL to browse to, in the tag			
		RelativeLayout.LayoutParams lpp = new RelativeLayout.LayoutParams(android.view.ViewGroup.LayoutParams.WRAP_CONTENT,android.view.ViewGroup.LayoutParams.WRAP_CONTENT);
		lpp.addRule(RelativeLayout.LEFT_OF,idOfOtherView);
		lpp.addRule(RelativeLayout.CENTER_IN_PARENT);
		browserImage.setLayoutParams(lpp);
		browserImage.setPadding(0, 0, 4, 0);
		return browserImage;		
	}
	
	/**
	 * Builds an ImageButton based on the specified parameters
	 * @param tag The string to be set in the objects "TAG"
	 * @return Returns the imagebutton built from the specified parameters
	 */
	private ImageButton getDeleteImage(String tag)
	{
		ButtonControl buttonDel = new ButtonControl(pressTypes.ASK_DELETE_PLACE_IN_MANAGE);
		ImageButton deleteImage = new ImageButton(this);
		deleteImage.setId(6);
		deleteImage.setImageResource(R.drawable.deleteicon);
		deleteImage.setBackgroundColor(Color.TRANSPARENT);
		deleteImage.setOnClickListener(buttonDel);
		deleteImage.setTag(tag);	//send name of place in the tag
		RelativeLayout.LayoutParams lp = new RelativeLayout.LayoutParams(android.view.ViewGroup.LayoutParams.WRAP_CONTENT,android.view.ViewGroup.LayoutParams.WRAP_CONTENT);
		lp.addRule(RelativeLayout.ALIGN_PARENT_RIGHT);
		lp.addRule(RelativeLayout.CENTER_IN_PARENT);
		deleteImage.setLayoutParams(lp);
		deleteImage.setPadding(0, 0, 0, 0);
		return deleteImage;
	}
	
	public LinearLayout getView()
	{
		return mainLayout;
	}
	
	@Override
	 public void onBackPressed()
	 {
		 LocationScreen.slideLeft(false);
		 buttonControl.onBackPressed();
	 }
	
	@Override
	 public boolean onCreateOptionsMenu(Menu menu)
	 {
		 return buttonControl.onCreateOptionsMenu(menu);
	 }
	
	 @Override
	 public boolean onOptionsItemSelected(MenuItem item)
	 {
		 return buttonControl.onOptionsItemSelected(item);
	 }
	 
	 @Override
	 public boolean onPrepareOptionsMenu(Menu menu)
	 {
		return buttonControl.onPrepareOptionsMenu(menu);		 
	 }
	 
	 public void removeAllViews()
	{
		if(null!=mainLayout){mainLayout.removeAllViews();}
		mainLayout=null;
	}
	 
	 public void setHeader()
	{
		customTitle = CustomTitle.getInstance(this);
		customTitle.buildHeader(headerString,imgID,mainLayout.getTitleTextColor());		
	}
}