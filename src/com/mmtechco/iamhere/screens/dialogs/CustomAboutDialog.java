package com.mmtechco.iamhere.screens.dialogs;

import com.mmtechco.iamhere.R;

import android.app.Dialog;
import android.content.Context;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager.NameNotFoundException;
import android.graphics.Color;
import android.graphics.Typeface;
import android.view.Window;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

public class CustomAboutDialog extends Dialog
{
	Context aboutContext;
	private static final float PADDING_IN_DP = 15.0f; // 1 dip = 1 pixel on an MDPI device
	private int mPaddingInPixels = 0;
	
	public CustomAboutDialog(Context _context)
	{
		super(_context);
		aboutContext = _context;
		requestWindowFeature(Window.FEATURE_NO_TITLE);
		buildGUI();
        //setContentView(R.layout.popup);
	}

	private void buildGUI()
	{	
		final float scale = aboutContext.getResources().getDisplayMetrics().density;
	    mPaddingInPixels = (int) (PADDING_IN_DP * scale + 0.5f);
		
		LinearLayout parentLayout = new LinearLayout(aboutContext);
		LinearLayout titleLayout  = new LinearLayout(aboutContext);
		LinearLayout titleLayoutInner  = new LinearLayout(aboutContext);
		LinearLayout bodyLayout   = new LinearLayout(aboutContext);
		
		parentLayout.setOrientation(LinearLayout.VERTICAL);
		titleLayoutInner.setOrientation(LinearLayout.VERTICAL);
		LinearLayout horizontalRule = new LinearLayout(aboutContext);
		horizontalRule.setMinimumHeight(2);
		horizontalRule.setBackgroundColor(Color.DKGRAY);

		
		ImageView iamhereImage = new ImageView(aboutContext);
		iamhereImage.setImageResource(R.drawable.icon);
		iamhereImage.setPadding(mPaddingInPixels,mPaddingInPixels,mPaddingInPixels,0);
		
		TextView titleText = new TextView(aboutContext);
		titleText.setText(R.string.Key);
		titleText.setPadding(mPaddingInPixels,mPaddingInPixels*2,mPaddingInPixels,0);
		titleText.setTypeface(Typeface.DEFAULT_BOLD);
		titleText.setTextSize(28);
		
		TextView verText = new TextView(aboutContext);
		verText.setText("ver: "+getVersionText());
		//titleText.setText(R.string.Key);
		verText.setPadding(mPaddingInPixels,0,0,mPaddingInPixels);
		verText.setTypeface(Typeface.DEFAULT_BOLD);
		verText.setTextSize(18);
		
		TextView aboutText = new TextView(aboutContext);
		aboutText.setText(R.string.about);
		aboutText.setPadding(mPaddingInPixels,0,mPaddingInPixels,mPaddingInPixels);
		aboutText.setTextColor(Color.WHITE);
		
		titleLayout.addView(iamhereImage);
		titleLayoutInner.addView(titleText);
		titleLayoutInner.addView(verText);
		titleLayout.addView(titleLayoutInner);
		bodyLayout.addView(aboutText);
		
		parentLayout.addView(titleLayout);
		parentLayout.addView(horizontalRule);
		parentLayout.addView(bodyLayout);
		parentLayout.setScrollContainer(true);
		setTitle(R.string.Key);
		
		setContentView(parentLayout);
	}
	
	private String getVersionText()
	{
		String strVersionName = "";
		try
		{
			PackageInfo packageInfo = aboutContext.getPackageManager().getPackageInfo(aboutContext.getPackageName(),0);

			//strVersionCode = "Version Code: "+ String.valueOf(packageInfo.versionCode);

			strVersionName = packageInfo.versionName;
		}
		catch (NameNotFoundException e)
		{
			//e.printStackTrace();
			//strVersionName = "Cannot load Version!";
		}
		return strVersionName;
	}
}