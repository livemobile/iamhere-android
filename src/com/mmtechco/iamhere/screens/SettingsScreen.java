package com.mmtechco.iamhere.screens;

import java.util.Locale;

import com.mmtechco.iamhere.ButtonControl;
import com.mmtechco.iamhere.R;
import com.mmtechco.iamhere.data.DataKeyValue;
import com.mmtechco.iamhere.enums.pressTypes;

import android.app.Activity;
import android.graphics.Color;
import android.graphics.LightingColorFilter;
import android.os.Bundle;
import android.view.Gravity;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewGroup.LayoutParams;
import android.widget.CheckBox;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.ScrollView;
import android.widget.TextView;

public class SettingsScreen extends Activity
{
	public final static String SHOW_SHARE_BOX = "showShareOnSocialScreen";

	private CustomTitle 	customTitle;
	private MainLayout 	 	mainLayout;
	private ButtonControl	buttonControl;
	private int 			screenWidth;
	private boolean			isRightToLeft;
	
	@Override
	public void onCreate(Bundle savedInstanceState) 
	{
		super.onCreate(savedInstanceState);
		buttonControl 	= new ButtonControl(this, pressTypes.WINDOW);
		
		mainLayout = new MainLayout(this);
		mainLayout.setOrientation(LinearLayout.VERTICAL);
		
		screenWidth	   = getWindowManager().getDefaultDisplay().getWidth();
		isRightToLeft=Locale.getDefault().getLanguage().equalsIgnoreCase("ar");
		
		setHeader();
		buildLayout();
		
		setContentView(mainLayout);
	}
	
	private void buildLayout()
	{
		mainLayout.addView(customTitle.getView());
		
		final int[] bgColour = mainLayout.getBgColour();
		final int txtColour  = mainLayout.getTextColour();
		final int separatorColour = mainLayout.getSeparatorColour();
		final int separatorthickness = 2;
		
		LinearLayout bodyLayout = new LinearLayout(this);

		bodyLayout.setOrientation(LinearLayout.VERTICAL);
		
		RelativeLayout.LayoutParams lpForText = new RelativeLayout.LayoutParams(LayoutParams.WRAP_CONTENT,LayoutParams.WRAP_CONTENT);
		lpForText.addRule(RelativeLayout.CENTER_VERTICAL);

		try 
		{
			bodyLayout.addView(getColourChange(R.string.Primary_Colour,txtColour,bgColour,pressTypes.SETTINGS_CLICK_COLOUR_TOP));
				bodyLayout.addView(getSeparator(separatorColour), new ViewGroup.LayoutParams(ViewGroup.LayoutParams.FILL_PARENT, separatorthickness)); 
			bodyLayout.addView(getColourChange(R.string.Secondary_Colour,txtColour,bgColour,pressTypes.SETTINGS_CLICK_COLOUR_BOTTOM));
				bodyLayout.addView(getSeparator(separatorColour), new ViewGroup.LayoutParams(ViewGroup.LayoutParams.FILL_PARENT, separatorthickness)); 
			bodyLayout.addView(getColourChange(R.string.Font_Colour,txtColour,new int[]{txtColour},pressTypes.SETTINGS_CLICK_COLOUR_FONT));
				bodyLayout.addView(getSeparator(separatorColour), new ViewGroup.LayoutParams(ViewGroup.LayoutParams.FILL_PARENT, separatorthickness)); 
		
		} 
		catch (IllegalAccessException e) 
		{	// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		bodyLayout.addView(getShareCheckBox(),new RelativeLayout.LayoutParams(
				LayoutParams.FILL_PARENT,
				LayoutParams.WRAP_CONTENT));
		
		bodyLayout.addView(getSeparator(separatorColour), new ViewGroup.LayoutParams(ViewGroup.LayoutParams.FILL_PARENT, separatorthickness)); 
		
		bodyLayout.addView(getAboutButton());//(viewAbout);		
		
		ScrollView sView = new ScrollView(this);
		sView.addView(bodyLayout);
		mainLayout.addView(sView);
	}

	private View getSeparator(int colour)
	{
		View hr1 = new View(this); 
		hr1.setBackgroundColor(colour);
		return hr1;
	}
	
	private View getColourChange(int rText, int txtColour, int[] colours, pressTypes CLICK_COLOUR) throws IllegalAccessException
	{
		RelativeLayout primColour = new RelativeLayout(this);
		
		if(CLICK_COLOUR == pressTypes.SETTINGS_CLICK_COLOUR_TOP
		|| CLICK_COLOUR == pressTypes.SETTINGS_CLICK_COLOUR_BOTTOM
		|| CLICK_COLOUR == pressTypes.SETTINGS_CLICK_COLOUR_FONT)
		{
			TextView colourTop = mainLayout.getNewTextView(rText);
			RelativeLayout.LayoutParams lpColourTop = new RelativeLayout.LayoutParams(LayoutParams.WRAP_CONTENT,LayoutParams.WRAP_CONTENT);
			lpColourTop.addRule( isRightToLeft?RelativeLayout.ALIGN_PARENT_RIGHT
											  :RelativeLayout.ALIGN_PARENT_LEFT);
			colourTop.setLayoutParams(lpColourTop);
			
			ImageButton colourImage = new ImageButton(this);
			colourImage.setMinimumWidth((screenWidth/5));
			
			int colourIndex = 0;
			if(CLICK_COLOUR == pressTypes.SETTINGS_CLICK_COLOUR_BOTTOM
			&& 1 < colours.length )
			{colourIndex = 1;}
			
			colourImage.getBackground().setColorFilter(new LightingColorFilter(Color.BLACK,colours[colourIndex]));
			colourImage.setOnClickListener(new ButtonControl(CLICK_COLOUR,colours));
	
			RelativeLayout.LayoutParams lpColourImage = new RelativeLayout.LayoutParams(LayoutParams.WRAP_CONTENT,LayoutParams.WRAP_CONTENT);
			lpColourImage.addRule( isRightToLeft?RelativeLayout.ALIGN_PARENT_LEFT
					  							:RelativeLayout.ALIGN_PARENT_RIGHT );
			colourImage.setLayoutParams(lpColourImage);
			
			primColour.setGravity(Gravity.CENTER_VERTICAL);
			primColour.addView(colourTop);
			primColour.addView(colourImage);
		}
		else
		{
			throw new IllegalAccessException("Invalid Press Type");
		}
		return primColour;
	}
	
	private View getShareCheckBox()
	{
		RelativeLayout showHideLayout = new RelativeLayout(this);		
		//Text description of option
		TextView viewShowHideShare = mainLayout.getNewTextView(R.string.showHideSettings);
		RelativeLayout.LayoutParams lpViewText = new RelativeLayout.LayoutParams(LayoutParams.WRAP_CONTENT,LayoutParams.WRAP_CONTENT);
		lpViewText.addRule( isRightToLeft?RelativeLayout.ALIGN_PARENT_RIGHT
										 :RelativeLayout.ALIGN_PARENT_LEFT);
		viewShowHideShare.setLayoutParams(lpViewText);
		//Checkbox to enable/disable option
		CheckBox showHideCheckBox = new CheckBox(this);
		showHideCheckBox.setOnCheckedChangeListener(new ButtonControl(pressTypes.SHOW_HIDE_CHECKED));
		RelativeLayout.LayoutParams lp = new RelativeLayout.LayoutParams(android.view.ViewGroup.LayoutParams.WRAP_CONTENT,android.view.ViewGroup.LayoutParams.WRAP_CONTENT);
		lp.addRule( isRightToLeft?RelativeLayout.ALIGN_PARENT_LEFT
								 :RelativeLayout.ALIGN_PARENT_RIGHT );
		showHideCheckBox.setLayoutParams(lp);
		//Set the checkbox to ticked/unticked depending on current option
		DataKeyValue dkv = new DataKeyValue(this);
		showHideCheckBox.setChecked(dkv.getValue(SHOW_SHARE_BOX).equalsIgnoreCase(DataKeyValue.TRUE)?true:false);
		
		showHideLayout.setGravity(Gravity.CENTER_VERTICAL);
		showHideLayout.addView(viewShowHideShare);
		showHideLayout.addView(showHideCheckBox);
		return showHideLayout;
	}
	
	private View getAboutButton()
	{
		RelativeLayout centerLayout = new RelativeLayout(this);
		centerLayout.setGravity(Gravity.CENTER);
		TextView viewAbout = mainLayout.getNewTextView(R.string.about_Button);

		centerLayout.setOnClickListener(new ButtonControl(pressTypes.SHOW_ABOUT_IN_SETTINGS));
		centerLayout.addView(viewAbout);
		return centerLayout;
	}
	
	@Override
	 public void onBackPressed()
	 {
		 buttonControl.onBackPressed();
	 }
	
	@Override
	 public boolean onCreateOptionsMenu(Menu menu)
	 {
		 return buttonControl.onCreateOptionsMenu(menu);
	 }
	
	 @Override
	 public boolean onOptionsItemSelected(MenuItem item)
	 {
		 return buttonControl.onOptionsItemSelected(item);
	 }
	 
	 @Override
	 public boolean onPrepareOptionsMenu(Menu menu)
	 {
		return buttonControl.onPrepareOptionsMenu(menu);		 
	 }
	 
	 public void setHeader()
	{
		int imgID		 = R.drawable.settingssmall;
		customTitle	 = CustomTitle.getInstance(this);
		customTitle.buildHeader(R.string.settings_Menu,imgID,mainLayout.getTitleTextColor());		
	}
}