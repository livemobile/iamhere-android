package com.mmtechco.iamhere.screens;

import com.mmtechco.iamhere.R;
import android.app.Activity;
import android.graphics.Color;
import android.graphics.Typeface;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

public class CustomTitle
{
	private RelativeLayout		titleLayout;
	private Activity 			activity;
	private int 				screenWidth;
	private static CustomTitle  customTitle;
	
	public static CustomTitle getInstance(Activity _activity)
	{
		if(null==customTitle)
		{
			customTitle=new CustomTitle(_activity);
		}
		return customTitle;
	}
	
	private CustomTitle(){}
	
	private CustomTitle(Activity _activity)
	{
		this();
		activity=_activity;
		//screenWidth = activity.getWindowManager().getDefaultDisplay().getWidth();
	}
	
	public void buildHeader(int subTitleText,int headerImageResource)
	{
		buildHeader(activity.getString(subTitleText),headerImageResource, Color.DKGRAY);
	}
	
	public void buildHeader(int subTitleText,int headerImageResource, int textColour)
	{
		buildHeader(activity.getString(subTitleText),headerImageResource, textColour);
	}
	
	public void buildHeader(String subTitleText,int headerImageResource)
	{
		buildHeader(activity.getString(R.string.Key),subTitleText,headerImageResource, Color.DKGRAY);
	}
	
	public void buildHeader(String subTitleText,int headerImageResource, int textColour)
	{
		buildHeader(activity.getString(R.string.Key),subTitleText,headerImageResource,textColour);
	}
	
	public void buildHeader(String titleText,String subTitleText,int headerImageResource)
	{
		buildHeader(titleText, subTitleText, headerImageResource, Color.DKGRAY);
	}
	
	/**
	 * Method for building the custom header at the top of each screen
	 * @param titleText Text to appear at the very top, typically the app name
	 * @param subTitleText Text to appear under title text
	 * @param headerImageResource The image to appear on the far right of the header
	 * @return Returns a View object containing the formatted header view
	 */
	public void buildHeader(String titleText,String subTitleText,int headerImageResource, int textColour)
	{

		/*
		LinearLayout outerLayout = new LinearLayout(activity);// outerLayout.setWeightSum(1.0f);
					 outerLayout.setLayoutParams(new LayoutParams(LayoutParams.FILL_PARENT,LayoutParams.WRAP_CONTENT));
		
		LinearLayout innerLayout = new LinearLayout(activity);
					 innerLayout.setWeightSum(1.0f);
					 innerLayout.setLayoutParams(new LayoutParams(LayoutParams.WRAP_CONTENT,LayoutParams.WRAP_CONTENT));
					
		RelativeLayout imageLayout = new RelativeLayout(activity);
					   imageLayout.setLayoutParams(new LayoutParams(LayoutParams.WRAP_CONTENT,LayoutParams.FILL_PARENT));
					   
					 
		TextView mainTitleTextView = new TextView(activity);
				 mainTitleTextView.setText(titleText);
				 mainTitleTextView.setLayoutParams(new LayoutParams(LayoutParams.WRAP_CONTENT,LayoutParams.WRAP_CONTENT));
				 
		TextView subTitleTextView = new TextView(activity);
				 subTitleTextView.setText(subTitleText);
				 subTitleTextView.setTypeface(Typeface.DEFAULT_BOLD);
				 subTitleTextView.setLayoutParams(new LayoutParams(LayoutParams.WRAP_CONTENT,LayoutParams.WRAP_CONTENT));
		
		ImageView headerImage = new ImageView(activity);
				  headerImage.setImageResource(headerImageResource);
				  headerImage.setLayoutParams(new LayoutParams(LayoutParams.WRAP_CONTENT,LayoutParams.WRAP_CONTENT));
				  

		imageLayout.addView(headerImage);
		
		innerLayout.setOrientation(LinearLayout.VERTICAL);
		innerLayout.addView(mainTitleTextView);
		innerLayout.addView(subTitleTextView);
		
		//outerLayout.addView(new View(activity),0);
		outerLayout.addView(imageLayout);
		outerLayout.addView(innerLayout);
		
		return outerLayout;
		*/
		titleLayout = new RelativeLayout(activity);
		RelativeLayout subTitleLayout = new RelativeLayout(activity);
		
		TextView mainTitleTextView = new TextView(activity);
				 mainTitleTextView.setText(titleText);
				 mainTitleTextView.setId(1);
		mainTitleTextView.setTextColor(textColour);
		TextView subTitleTextView = new TextView(activity);
				 subTitleTextView.setText(subTitleText);
				 subTitleTextView.setTypeface(Typeface.DEFAULT_BOLD);
				 subTitleTextView.setId(2);
				 subTitleTextView.setPadding(0, 0, screenWidth/10, 0);
					subTitleTextView.setTextColor(textColour);
		RelativeLayout.LayoutParams lpp = new RelativeLayout.LayoutParams(android.view.ViewGroup.LayoutParams.WRAP_CONTENT,android.view.ViewGroup.LayoutParams.WRAP_CONTENT);
		lpp.addRule(RelativeLayout.BELOW,mainTitleTextView.getId());
		subTitleTextView.setLayoutParams(lpp);
		
		ImageView headerImage = new ImageView(activity);
		headerImage.setImageResource(headerImageResource);
		//headerImage.setScaleType(ScaleType.FIT_XY);		
		//headerImage.setPadding(screenWidth/10, 0, 0, 0);
		
		RelativeLayout.LayoutParams lp = new RelativeLayout.LayoutParams(android.view.ViewGroup.LayoutParams.WRAP_CONTENT,android.view.ViewGroup.LayoutParams.WRAP_CONTENT);
		lp.addRule(RelativeLayout.ALIGN_PARENT_RIGHT);
		lp.addRule(RelativeLayout.ALIGN_PARENT_TOP);		
		headerImage.setLayoutParams(lp);
		
		subTitleLayout.addView(mainTitleTextView);
		subTitleLayout.addView(subTitleTextView);
		
		titleLayout.addView(subTitleLayout);
		titleLayout.addView(headerImage);
		//headerImage.setMaxHeight(mainTitleTextView.getHeight()+subTitleTextView.getHeight());
	}
	
	public RelativeLayout getView()
	{
		return titleLayout;
	}
}