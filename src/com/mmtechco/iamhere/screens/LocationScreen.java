package com.mmtechco.iamhere.screens;

import com.mmtechco.iamhere.ButtonControl;
import com.mmtechco.iamhere.Gui;
import com.mmtechco.iamhere.R;
import com.mmtechco.iamhere.enums.Directions;
import com.mmtechco.iamhere.enums.ScreenType;
import com.mmtechco.iamhere.enums.pressTypes;

import android.app.Activity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.LinearLayout;

public class LocationScreen extends Activity
{
	private CustomTitle 	 customTitle;
	private ContactList 	 contactList;
	private String			 headerString;
	private int				 imgID;
	private MainLayout 	 	 mainLayout;
	private ButtonControl	 buttonControl;
	private static boolean	 slideLeft;
	
	@Override
	public void onCreate(Bundle savedInstanceState) 
	{
		super.onCreate(savedInstanceState);
		
		getWindow().setWindowAnimations(android.R.style.Animation);

		buttonControl = new ButtonControl(this, pressTypes.WINDOW);
		
		mainLayout = new MainLayout(this);
		mainLayout.setOrientation(LinearLayout.VERTICAL);
		
		setHeader();
		buildLayout();
		
		if(slideLeft)
		{	UIHelper.setSlideDirection(mainLayout, Directions.LEFT);	}
		else
		{	UIHelper.setSlideDirection(mainLayout, Directions.RIGHT);	}
		
		setContentView(mainLayout);
		Gui.getInstance(this).showNewPlaces();
	}
	
	/**
	 * Sets whether or not the screen animation should slide left or not.
	 * @param left Boolean true means slide left, false means slide right
	 */
	public static void slideLeft(boolean left)
	{
		slideLeft=left;
	}

	private void buildLayout()
	{
		//Gui.getInstance(this);
		contactList = ContactList.getInstance(this,mainLayout.getTextColour());
		mainLayout.addView(customTitle.getView());
		mainLayout.addView(contactList.buildList());
		Gui.setMode(ScreenType.LOCATION);//Gui.setMode(true,false,false,false);
	}
	
	public LinearLayout getView()
	{
		return mainLayout;	
	}
	
	@Override
	 public void onBackPressed()
	 {
		 buttonControl.onBackPressed();
	 }
	
	@Override
	 public boolean onCreateOptionsMenu(Menu menu)
	 {
		 return buttonControl.onCreateOptionsMenu(menu);
	 }
	
	 @Override
	 public boolean onOptionsItemSelected(MenuItem item)
	 {
		 return buttonControl.onOptionsItemSelected(item);
	 }
	 
	 @Override
	 public boolean onPrepareOptionsMenu(Menu menu)
	 {
		return buttonControl.onPrepareOptionsMenu(menu);		 
	 }
	 
	 public void removeAllViews()
	{
		if(null!=mainLayout)
		{mainLayout.removeAllViews();}
	}
	 
	 public void setHeader()
	{
		headerString = getString(R.string.yesLoc);
		imgID		 = R.drawable.currentplacesmall;
		customTitle	 = CustomTitle.getInstance(this);
		customTitle.buildHeader(headerString,imgID,mainLayout.getTitleTextColor());		
	}
}