package com.mmtechco.iamhere.interfaces;

import android.location.Location;

public interface CallBack {

	void buildOutContacts(Location loc);

}
