package com.mmtechco.iamhere.data;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

public class DataManager extends SQLiteOpenHelper
{
	private final static String DB_TABLE = "placesTable";
	private final static String KEY_LAT  = "Latitude";
	private final static String KEY_LON  = "Longitude";
	private final static String KEY_NAME = "PlaceName";
	private final static String KEY_TIME = "time";
	private final static String KEY_NEW  = "new";
	
	public  final static int maxChar_Place   = 20;
	public  final static int maxChar_Message = 60;
	
	private ContentValues initialValues;	
	private static SQLiteDatabase storeDB;
	
	public DataManager(Context context)
	{
		super(context, DB_TABLE, null, 1);
		initialValues = new ContentValues();
		if(null == storeDB)
		{storeDB = getWritableDatabase();}		
	}
	
/*	public DataManager()
	{	
		if(null == storeDB)
		{throw new Exception("storeDB not created yet");}	
	}*/

	@Override
	public void onCreate(SQLiteDatabase db)
	{
		StringBuilder Query = new StringBuilder();
		Query.append("CREATE TABLE IF NOT EXISTS ");
		Query.append(DB_TABLE);
		Query.append('(');
		Query.append(KEY_LAT);
			Query.append(" FLOAT,");
		Query.append(KEY_LON);
			Query.append(" FLOAT,");	
		Query.append(KEY_NAME);
			Query.append(" NVARCHAR("+maxChar_Place+") PRIMARY KEY,");
		Query.append(KEY_TIME);
			Query.append(" INT,");	
		Query.append(KEY_NEW);
			Query.append(" BOOLEAN DEFAULT TRUE);");
			
		db.execSQL(Query.toString());
	}
	
	public boolean addEntry(String lat, String lon, String placeName)
	{
		return addEntry(lat, lon, placeName, true);
	}
	
	public boolean addEntry(float lat, float lon, String placeName)
	{
		return addEntry(lat, lon, placeName,true);
	}
	
	public boolean addEntry(float lat, float lon, String placeName, boolean newEntry)
	{
		return addEntry(String.valueOf(lat), String.valueOf(lon), placeName, newEntry);
	}
	
	public boolean addEntry(String lat, String lon, String placeName, boolean newEntry)
	{
		boolean result = false;
		
		if(false == placeExists(placeName))
		{
			initialValues.clear();
	        initialValues.put(KEY_TIME, System.currentTimeMillis());
	        initialValues.put(KEY_LAT,  lat);
	        initialValues.put(KEY_LON,  lon);
	        initialValues.put(KEY_NAME, placeName);
	        initialValues.put(KEY_NEW,  booleanToString(newEntry));//new Boolean(newEntry).toString());
	        
	        result = storeDB.insert(DB_TABLE, null, initialValues) > -1 ? true:false;
		}
		
		return result;
	}
	
	public boolean placeExists(String placeName)
	{
		boolean result = false;

		Cursor c = storeDB.query(DB_TABLE, new String[]{KEY_NAME}, KEY_NAME +" LIKE ?", new String[]{placeName}, null, null, KEY_NAME);
		
		if(c.getCount()>0)
		{	result = true;	}
		//Log.v("987654","looking for "+placeName+" : " +c.getCount());
		c.close();
		return 	result;
	}
	
	public String[] getNewPlaces()
	{
		return (String[])getPlaces(true,false);
	}
	
	public String[] getAllPlaces()
	{
		return (String[])getPlaces(false,false);
	}
	
	public String[][] getAllPlacesInfo()
	{
		return(String[][])getPlaces(false,true);
	}
		
	private Object getPlaces(boolean onlyNewEntries, boolean return2D)
	{
		//If we want all places, then we're probably in the manage places section,
		//in which case we want more details than just the name. We also need coords for URL link.
		String[] columnsToRetrieve = (onlyNewEntries
										? new String[]{KEY_NAME}
										: new String[]{KEY_NAME,KEY_LAT,KEY_LON});
		
		Cursor c = storeDB.query(DB_TABLE, columnsToRetrieve, KEY_NEW + " LIKE ?", new String[]{booleanToString(onlyNewEntries)}, null, null, KEY_NAME);
		//Cursor c = storeDB.query(DB_TABLE, new String[]{KEY_NAME}, KEY_NEW + " LIKE 'false'", null, null, null, KEY_NAME);

		if(return2D)
		{
			String[][] resultArray = new String[c.getCount()][3];
			c.moveToFirst();
			
			for(int count = 0; resultArray.length > count; count++)
			{
				resultArray[count][0] = c.getString(0);
				//Log.i("iamhere","array0: "+resultArray[count][0]);
				resultArray[count][1] = c.getString(1);
				//Log.i("iamhere","array1: "+resultArray[count][1]);
				resultArray[count][2] = c.getString(2);
				//Log.i("iamhere","array2: "+resultArray[count][2]);
				c.moveToNext();
			}
			c.close();
			return resultArray;
		}
		else
		{
		String[] resultArray = new String[c.getCount()];
		c.moveToFirst();
		
		for(int count = 0; resultArray.length > count; count++)
		{
			resultArray[count] = c.getString(0);
			c.moveToNext();
		}
		c.close();
		return resultArray;
		}
	}
	
	public void saveNewPlaces(String placeName)
	{
		//1st set all 
		initialValues.clear();
        initialValues.put(KEY_NEW, "FALSE");
        
		storeDB.update(DB_TABLE, initialValues, KEY_NAME + " LIKE ?", new String[]{placeName});
	}
	
	public void deleteUnsavedPlaces()
	{
		String[] places = getNewPlaces();
		for(String place: places)
		{deletePlace(place);}
	}
	
	public boolean deletePlace(String placeName)
	{
		return storeDB.delete(DB_TABLE, KEY_NAME + " LIKE ?", new String[]{placeName}) > 0?true:false;
	}
	
	public float[] getLocForPlaceName(String placeName)
	{
		Cursor c = storeDB.query(DB_TABLE, new String[]{KEY_LAT,KEY_LON}, KEY_NAME + " LIKE ?", new String[]{placeName}, null, null, null);
		c.moveToFirst();
		float[] gps = new float[0];
		if(c.getCount()>0)
		{	gps = new float[]{c.getFloat(0),c.getFloat(1)};}
		c.close();
		return gps;
	}
	
	public boolean updatePlaceName(String oldName, String newName)
	{
		initialValues.clear();
        initialValues.put(KEY_NAME, newName);
        return storeDB.update(DB_TABLE, initialValues, KEY_NAME + " LIKE ?", new String[]{oldName})>0?true:false;
	}

	//Float.valueOf(latText.replace('?', ' ').trim()).floatValue()
	public boolean updateEntry(String lat, String lon, String placeName)
	{
		initialValues.clear();
        initialValues.put(KEY_LAT, lat);
        initialValues.put(KEY_LON, lon);
        return storeDB.update(DB_TABLE, initialValues, KEY_NAME + " LIKE ?", new String[]{placeName})>0?true:false;
	}
	
	@Override
	public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion)
	{
		// TODO Auto-generated method stub		
	}
	
	private String booleanToString(boolean val)
	{
		if(val)
		{	return "TRUE";	}
		else
		{	return "FALSE";	}
	}
}