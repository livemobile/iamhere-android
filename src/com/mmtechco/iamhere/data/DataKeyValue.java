package com.mmtechco.iamhere.data;

import com.mmtechco.iamhere.screens.MainLayout;
import com.mmtechco.iamhere.screens.SettingsScreen;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteException;
import android.database.sqlite.SQLiteOpenHelper;
import android.graphics.Color;

public class DataKeyValue extends SQLiteOpenHelper
{
	private final static String DB_TABLE = "Vals";
	private final static String KEY  	 = "k";
	private final static String VALUE    = "v";
	public  final static String TRUE     = "TRUE";
	public  final static String FALSE    = "FALSE";
	private ContentValues initialValues;
	
	private static SQLiteDatabase storeDB;
	
	/*private static DataKeyValue dkv;
	
	public static DataKeyValue getInstance(Context context)
	{
		if(null != dkv)
		{dkv.close();}
		
		dkv = new DataKeyValue(context);
		return dkv;
	}
	
	private*/ public DataKeyValue(Context context)
	{
		super(context, DB_TABLE, null, 1);
		initialValues = new ContentValues();
		if(null != storeDB)
		{storeDB.close();}storeDB = getWritableDatabase();
		if(tableEmpty())
		{setDefault();}
	}

	@Override
	public void onCreate(SQLiteDatabase db)
	{
		StringBuilder Query = new StringBuilder();
		Query.append("CREATE TABLE IF NOT EXISTS ");
		Query.append(DB_TABLE);
		Query.append('(');
		Query.append(KEY);
			Query.append(" TEXT PRIMARY KEY,");
		Query.append(VALUE);
			Query.append(" TEXT);");
		db.execSQL(Query.toString());
	}
	
	private void setDefault()
	{
		setValue(MainLayout.COLOUR_TOP,Color.rgb(254, 219, 65)+"");
		setValue(MainLayout.COLOUR_BOTTOM,Color.rgb(248, 150, 29)+"");
		setValue(MainLayout.COLOUR_TEXT,Color.WHITE+"");
		setValue(SettingsScreen.SHOW_SHARE_BOX,TRUE);
	}
	
	public int getValueAsINT(String _key)
	{
		return Integer.parseInt(getValue(_key));
	}
	
	public String getValue(String _key)
	{
		Cursor c=null;
		String val = "";

		try
		{
		//Andrew: I think this line is causing the problem in settings, The database is already open when this is called
		//c = getReadableDatabase().query(DB_TABLE, new String[]{VALUE}, KEY + " LIKE ?", new String[]{_key}, null, null, null);
		//replaced with
		c = storeDB.query(DB_TABLE, new String[]{VALUE}, KEY + " LIKE ?", new String[]{_key}, null, null, null);
		c.moveToFirst();
		if(c.getCount()>0)
			{	val = c.getString(0);	}
		}
		catch(SQLiteException e)
		{
			//Log.e("iamhere","getValue::SQLiteException::"+e.getMessage());
			e.printStackTrace();
		}
		finally
		{
			c.close();
		}
		return val;
	}
	
	public void setValue(String _key, String _value)
	{
		initialValues.clear();
        initialValues.put(VALUE, _value);
        
		if(keyExists(_key))
		{    	
         storeDB.update(DB_TABLE, initialValues, KEY + " LIKE ?", new String[]{_key});
		}
		else
		{
			initialValues.put(KEY, _key);
	        storeDB.insert(DB_TABLE, null, initialValues);
		}
	}
	
	private boolean tableEmpty()
	{
		boolean result = false;

		Cursor c = storeDB.query(DB_TABLE, null, null, null, null, null, null);
		if(c.getCount() == 0)
		{	result = true;	}
		c.close();
		return 	result;
	}
	
	private boolean keyExists(String _key)
	{
		boolean result = false;

		Cursor c = storeDB.query(DB_TABLE, new String[]{KEY}, KEY +" LIKE ?", new String[]{_key}, null, null, null);
		if(c.getCount()>0)
		{	result = true;	}
		c.close();
		return 	result;
	}
/*
	@Override
	public void finalize()
	{close();}
	*/
	@Override
	public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) 
	{}	// TODO Auto-generated method stub	
}
