/*
 * Copyright (C) 2010 Daniel Nilsson
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.mmtechco.iamhere.mColorPicker;

import com.mmtechco.iamhere.R;
import com.mmtechco.iamhere.mColorPicker.views.ColorPanelView;
import com.mmtechco.iamhere.mColorPicker.views.ColorPickerView;
import com.mmtechco.iamhere.mColorPicker.views.ColorPickerView.OnColorChangedListener;

import android.app.AlertDialog;
import android.content.Context;
import android.graphics.Color;
import android.graphics.PixelFormat;
import android.view.Gravity;
import android.view.ViewGroup.LayoutParams;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

public class ColorPickerDialog extends AlertDialog implements ColorPickerView.OnColorChangedListener 
{
	private Context			context;
	private ColorPickerView mColorPicker;
	private ColorPanelView  mOldColor;
	private ColorPanelView  mNewColor;
	private OnColorChangedListener mListener;
    private int				mInitialColor;
    private int 			mDefaultColor;
	
	//public ColorPickerDialog(Context _context, OnColorChangedListener listener, int initialColor)
    public ColorPickerDialog(Context _context, int initialColor)
	{
		super(_context);
		mListener = this;
		context = _context;
		
        mInitialColor = initialColor;
        mDefaultColor = initialColor;	// this was being passed as another var in ctr but both the same so...
		
		mColorPicker  = new ColorPickerView(context);
		mOldColor	  = new ColorPanelView(context);
		mNewColor	  = new ColorPanelView(context);
		
		init();
	}

	private void init()
	{
		// To fight color branding.
		getWindow().setFormat(PixelFormat.RGBA_8888);

		setUp();
	}

	private void setUp()
	{
		//LayoutInflater inflater = (LayoutInflater) getContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
		RelativeLayout layout = buildDialogColorPicker();//= inflater.inflate(R.layout.dialog_color_picker, null);

		setView(layout);

		setTitle(R.string.pickacolour);
		// setIcon(android.R.drawable.ic_dialog_info);
		
		((LinearLayout) mOldColor.getParent()).setPadding(Math.round(mColorPicker.getDrawingOffset()),0,
														  Math.round(mColorPicker.getDrawingOffset()),0);

		mColorPicker.setOnColorChangedListener(mListener);

		mOldColor.setColor(mInitialColor);
		mNewColor.setColor(mDefaultColor);
		mColorPicker.setColor(mInitialColor, true);		
	}

	private RelativeLayout buildDialogColorPicker() 
	{
		RelativeLayout layout = new RelativeLayout(context);
		layout.setLayoutParams(new RelativeLayout.LayoutParams(LayoutParams.FILL_PARENT,LayoutParams.FILL_PARENT));
		
		mColorPicker.setId(1);
		RelativeLayout.LayoutParams lp1 = new RelativeLayout.LayoutParams(LayoutParams.FILL_PARENT,LayoutParams.WRAP_CONTENT);
		lp1.addRule(RelativeLayout.CENTER_HORIZONTAL);
		lp1.setMargins(10, 0, 10, 0);
		mColorPicker.setLayoutParams(lp1);
		layout.addView(mColorPicker);

		LinearLayout linLayout1 = new LinearLayout(context);
		RelativeLayout.LayoutParams lp2 = new RelativeLayout.LayoutParams(LayoutParams.FILL_PARENT,LayoutParams.WRAP_CONTENT);
		lp2.addRule(RelativeLayout.BELOW,mColorPicker.getId());
		lp2.addRule(RelativeLayout.ALIGN_LEFT,mColorPicker.getId());
		lp2.addRule(RelativeLayout.ALIGN_RIGHT,mColorPicker.getId());
		lp2.setMargins(0, 10, 0, 0);
		linLayout1.setLayoutParams(lp2);
		
		LinearLayout.LayoutParams rlp1 = new LinearLayout.LayoutParams(LayoutParams.WRAP_CONTENT,LayoutParams.FILL_PARENT,0.5f);
		rlp1.width=0; //TODO: Really??
		mOldColor.setLayoutParams(rlp1);
		linLayout1.addView(mOldColor);
		
		TextView tv1 = new TextView(context);
		LinearLayout.LayoutParams tvlp = new LinearLayout.LayoutParams(LayoutParams.WRAP_CONTENT,LayoutParams.FILL_PARENT);
		tvlp.setMargins(10, 0, 10, 0);
		tv1.setLayoutParams(tvlp);
		tv1.setTextSize(20);
		tv1.setGravity(Gravity.CENTER);
		tv1.setTextColor(Color.WHITE);
		tv1.setText("->");
		linLayout1.addView(tv1);

		mNewColor.setLayoutParams(rlp1);	// LayoutParams alredy defined above
		linLayout1.addView(mNewColor);

		layout.addView(linLayout1);
		return layout;
	}

	public void updateColour(int colour)
	{
		if (mListener != null)
		{
			mListener.onColorChanged(colour);
		}
	}
	
	@Override
	public void onColorChanged(int colour)
	{
		//Log.i("iamhere","in onColourChanged::ColorPickerDialog::"+colour);
		mNewColor.setColor(colour);

		/*
		if (mListener != null)
		{
			mListener.onColorChanged(colour);
		}
		*/
	}

	public void setAlphaSliderVisible(boolean visible) {
		mColorPicker.setAlphaSliderVisible(visible);
	}

	public int getNewColour()
	{
		//Log.i("iamhere","CPD::getNewColour::"+mNewColor.getColor());
		return mNewColor.getColor();
	}
	public int getColor() {
		return mColorPicker.getColor();
	}
}