package com.mmtechco.iamhere;

import java.util.List;

import com.mmtechco.iamhere.contacts.ContactTarget;
import com.mmtechco.iamhere.data.DataManager;
import com.mmtechco.iamhere.enums.ScreenType;
import com.mmtechco.iamhere.enums.pressTypes;
import com.mmtechco.iamhere.mColorPicker.ColorPickerDialog;
import com.mmtechco.iamhere.screens.ForwardScreen;
import com.mmtechco.iamhere.screens.LocationScreen;
import com.mmtechco.iamhere.screens.ManageScreen;
import com.mmtechco.iamhere.screens.SettingsScreen;
import com.mmtechco.iamhere.screens.dialogs.CustomAboutDialog;
import com.mmtechco.iamhere.social.Social;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.app.AlertDialog.Builder;
import android.content.Context;
import android.content.DialogInterface;
import android.content.DialogInterface.OnCancelListener;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.content.pm.PackageManager.NameNotFoundException;
import android.content.pm.ResolveInfo;
import android.location.LocationManager;
import android.net.ConnectivityManager;
import android.net.Uri;
import android.os.CountDownTimer;
import android.provider.Settings;
import android.text.InputFilter;
import android.view.ContextThemeWrapper;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

public class Gui 
{	
	private static ScreenType mode;
	private ProgressDialog 	  myDialog;
	private EditText		  placeNameBox;
	private EditText 		  messageBodyBox;
	private CustomAboutDialog cad;
	private Activity 		  activity;
	private Activity 		  topActivity;
	private DataManager 	  dm;
	private LocationManager   locMan;
	private static Gui		  display;
	private Builder 		  newPlacesDialog;
	private Social			  social;
	public  ColorPickerDialog colorPickDialog;
	private AlertDialog 	  gpsAlert;

	public static Gui getInstance(Activity _activity)
	{
		if(null==display)
		{	display = new Gui(_activity);	}
		else
		{	display.activity = _activity;	}
		
		return getInstance();
	}
	
	public static Gui getInstance()
	{
		return display;
	}
	
	private Gui()
	{}
	
	private Gui(Activity _activity)
	{
		this();		
		activity    = _activity;
		topActivity =  activity;
		dm     = new DataManager(activity);
		locMan = (LocationManager) activity.getSystemService(Context.LOCATION_SERVICE );
		social = new Social(activity);
	}

	public boolean isForwardMode()
	{
		return ScreenType.FORWARD == mode?true:false;//return forwardMode;
	}	 
	public boolean isLocationMode()
	{
		return ScreenType.LOCATION == mode?true:false;//return locationMode;
	}
	public boolean isManageMode()
	{
		return ScreenType.MANAGE == mode?true:false;//return manageMode;
	}
	public boolean isSettingsMode()
	{
		return ScreenType.SETTINGS == mode?true:false;//return settingsMode;
	}
	/**
	 * Sets which screen is currently active
	 * @param location Is location mode active?
	 * @param forward Is forward mode active?
	 * @param manage Is manage mode active?
	 * @param settings Is settings mode active?
	 */
	public static void setMode(ScreenType _mode)
	{		
		mode = _mode;
	} 
	 
	 private Builder buildSendDialog(boolean withMessageBox)
	 {
 		 Builder dialog = new AlertDialog.Builder(new ContextThemeWrapper(activity, android.R.style.Theme_Dialog));
		 dialog.setInverseBackgroundForced(true);

		 LinearLayout layout  = new LinearLayout(activity);
         layout.setOrientation(LinearLayout.VERTICAL);  
         
         placeNameBox = new EditText(activity);
         //Create text field for user to enter location description
		 placeNameBox.setSingleLine();
		 placeNameBox.setFilters(new InputFilter[]{new InputFilter.LengthFilter(DataManager.maxChar_Place)});
		 placeNameBox.setHint(R.string.placeName);
		 placeNameBox.setText(ContactTarget.place);

         layout.addView(placeNameBox);
         
         if(withMessageBox)
         {
	         messageBodyBox = new EditText(activity);
	         messageBodyBox.setFilters(new InputFilter[]{new InputFilter.LengthFilter(DataManager.maxChar_Message)});
	 		 messageBodyBox.setHint(R.string.placeMessage);
	 		 
	 		 layout.addView(messageBodyBox);
         }
         
         //Set the view that the dialog will show
         dialog.setView(layout);
         
         return dialog;
	 }
	 
	 /**
	 * Method checks to see if GPS is enabled.
	 * If not, it will open the options menu so the user can enable it.
	 */
	public boolean checkIfGPSisEnabled()
	{
		boolean result = false;
		ConnectivityManager cm = (ConnectivityManager)activity.getSystemService(Context.CONNECTIVITY_SERVICE);
		
		boolean gpsON  = locMan.isProviderEnabled(LocationManager.GPS_PROVIDER);
		boolean tickBX = locMan.isProviderEnabled(LocationManager.NETWORK_PROVIDER);
		boolean wifiON = (null==cm.getActiveNetworkInfo()?false:true);

	    if ( !gpsON && (!wifiON || !tickBX))
	    {
	    	if (null==gpsAlert)
	    	{
	        	gpsAlert=new AlertDialog.Builder(activity).create();
		    	gpsAlert.setCancelable(false);
		    	gpsAlert.setMessage(activity.getString(R.string.GPSEnabled));
		    	gpsAlert.setButton(activity.getString(R.string.yes), new DialogInterface.OnClickListener()
		    	   {   @Override
		    		   public void onClick(final DialogInterface dialog, final int id)
		    		   {
		    		   	   gpsAlert.dismiss();
		    		       removeWaitingForGPS();
		    			   Intent intent = new Intent(Settings.ACTION_LOCATION_SOURCE_SETTINGS);
		    			   activity.startActivityForResult(intent, 0); 
		    		   }
		    	   });
		    	gpsAlert.setButton2(activity.getString(R.string.no), new ButtonControl(pressTypes.CANCEL_GPS_YES_NO));
		    }
	    	gpsAlert.show();
	    	/*
	    	AlertDialog.Builder builder = new AlertDialog.Builder(activity);
	    	builder.setMessage(R.string.GPSEnabled)
	    	   .setCancelable(false)
	    	   .setPositiveButton(R.string.yes, new DialogInterface.OnClickListener()
	    	   {   @Override
	    		   public void onClick(final DialogInterface dialog, final int id)
	    		   {
	    		   	   gpsAlert.dismiss();
	    		       removeWaitingForGPS();
	    			   Intent intent = new Intent(Settings.ACTION_LOCATION_SOURCE_SETTINGS);
                       activity.startActivityForResult(intent, 0); 
	    		   }
	    	   })
	    	   .setNegativeButton(R.string.no, new ButtonControl(pressTypes.CANCEL_GPS_YES_NO));
	    	gpsAlert = builder.create();
	    	gpsAlert.show();*/
	    }
	    else
	    {
	    	result=true;
	    }
		return result;
	}
	 
	
	 public void endActivity()
	{
		activity.finish();
		topActivity.finish();
		display = null;
	}
	 	 
	 @Override
	 public void finalize()
	 {
		 if(null != cad)
		 	{cad.dismiss();}
		 
		 if(null != gpsAlert)
		 {
			 //Log.i("iamhere","dismissing GPS alert");
			 gpsAlert.dismiss();
			 //gpsAlert.cancel();
			 gpsAlert=null;
		 }
		 
		 if(null!=myDialog)
		 {
			 myDialog.dismiss();
			 myDialog=null;
		 }

		 endActivity();
	 }
	 
	 public String getMessage()
	 {
		 ContactTarget.message = messageBodyBox.getText().toString();
		 return ContactTarget.message;
	 }
	 
	 public String getPlaceName()
	 {
		 ContactTarget.place = placeNameBox.getText().toString();
		 return ContactTarget.place;
	 }
	
	/**
	 * Depending on which screen we are in, we only want certain options to appear in the menu.
	 * This method handles that by disabling certain menu items.
	 * @param menu The menu object
	 * @return true if handled successfully, false otherwise
	 */
	public boolean refreshFooterMenu(Menu menu)
	{
		boolean  handled	 = false;
		MenuItem savePlace   = menu.findItem(R.id.saveThisPlaces);
	    MenuItem sharePlace  = menu.findItem(R.id.shareplaces);
	    MenuItem managePlace = menu.findItem(R.id.manageplaces);
	    MenuItem settings 	 = menu.findItem(R.id.settings);
	    
	    switch(mode)
	    {
		    case LOCATION:
		    	 savePlace.setEnabled(true);
		    	 sharePlace.setEnabled(true);
		    	 managePlace.setEnabled(true);
		    	 settings.setEnabled(true);
		    	 handled=true;
		    	 break;
		    case FORWARD:
		    	 sharePlace.setEnabled(false);
		    	 handled=true;
		    	 break;
		    case MANAGE:
		    	 managePlace.setEnabled(false);
		    	 handled=true;
		    	 break;
		    case SETTINGS: 
		    	 settings.setEnabled(false);
		    	 handled=true;
				 break;
	    }
	    
		return handled;
	}
			
	public void removeWaitingForGPS()
	{
		 if(myDialog != null)
		 { 
			myDialog.dismiss();
		 	myDialog = null;
		 }
	}
	
	/**
	 * On screen orientation change (and when app loads), this method gets called.
	 * This is here to redraw the screen after the orientation change.
	 * It can also be used to redraw the screen that the app is meant to be displaying.
	 */
	public void restoreScreen()
	{
		activity.finish();
		if(isLocationMode())
		{
			Intent i = new Intent(activity,LocationScreen.class);
			i.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);			
			activity.startActivity(i);
		}
		else if (isManageMode())
		{
			Intent i = new Intent(activity,ManageScreen.class);
			i.setFlags(Intent.FLAG_ACTIVITY_PREVIOUS_IS_TOP);
			activity.startActivityForResult(i,0);
		}
		else if (isForwardMode())
		{
			Intent i = new Intent(activity,ForwardScreen.class);
			i.setFlags(Intent.FLAG_ACTIVITY_PREVIOUS_IS_TOP);
			activity.startActivityForResult(i,0);
		}		
	}
	
	/**
	 * Method pops up a text box for user to name the current set of lat/lon coords.
	 */
	public void saveCurrentPlace()
	{
		 Builder dialog = buildSendDialog(false);
		 
 		 ButtonControl sendControl = new ButtonControl(pressTypes.SAVE_THIS_PLACE);
 		
 	     //Set the title at the top of the popup dialog
         dialog.setTitle(R.string.saveCurrentPlace);
         
         //What do we do when the "Send" button is clicked
         dialog.setPositiveButton( R.string.Save, sendControl);
         //Using null as the 2nd argument here makes it do nothing, ie Dismiss or Cancel the dialog
         dialog.setNegativeButton(R.string.cancel, null);
         
         dialog.show();
	}

	/**
	 * This method searches through the array of place names that are in the "Manage" screen.
	 * It then compares them to the currently stored place names in the database.
	 * If there are any names in the updated places array that do not match, then
	 * the DB is updated with this new name.
	 * @param updatedPlaces An array of place names taken from the "Manage Places" screen.
	 */
	public void saveEditedPlace(String[] updatedPlaces)
	{
		boolean anyChangesMade = false;
		boolean successfullyEditPlace=false;
		
		String[] allPlaces = dm.getAllPlaces();
		for(int count=0 ; count<allPlaces.length ; count++)
		{
			if(!allPlaces[count].equals(updatedPlaces[count]))
			{
				successfullyEditPlace = dm.updatePlaceName(allPlaces[count], updatedPlaces[count]);
				anyChangesMade=true;
			}	
		}
		if(anyChangesMade)
		{
			if(successfullyEditPlace)
			{
				Toast.makeText(activity, R.string.changesSaved, Toast.LENGTH_SHORT).show();
			}
			else
			{
				Toast.makeText(activity, R.string.failedToSave, Toast.LENGTH_SHORT).show();
			}
			/*
			AlertDialog.Builder builder = new AlertDialog.Builder(activity);
			if(successfullyEditPlace)
			{
				builder.setMessage(R.string.changesSaved)
				       .setNeutralButton(R.string.ok, null)
				       .show();
			}
			else
			{
				builder.setMessage(R.string.failedToSave)
				       .setNeutralButton(R.string.ok, null)
				       .show();
			}
			*/
		}
		else
		{
			Toast.makeText(activity, R.string.No_changes_made, Toast.LENGTH_SHORT).show();
		}
		
		switchToMyLocationMode();
		// Now hide the soft keyboard
		//activity.getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_HIDDEN);
	}
	
	/**
	 * Displays the "About" dialog box
	 */
	public void showAbout()
	{
		cad = new CustomAboutDialog(activity);
		cad.show();
	}
	
	public void showDeleteConfirmation(String placeName)
	{
		//Log.i("iamhere","showDeleteConfirmation: "+placeName);
		AlertDialog.Builder builder = new AlertDialog.Builder(activity);
		builder.setMessage(activity.getString(R.string.deletePlace)+" \""+placeName+"\"?")
			   .setPositiveButton(R.string.delete, new ButtonControl(pressTypes.DO_DELETE_PLACE_IN_MANAGE,placeName))
			   .setNegativeButton(R.string.cancel, null)
			   .show();
	}
	
	public boolean showFooterMenu(Menu menu)
	{		
	    MenuInflater inflater = activity.getMenuInflater();
	    inflater.inflate(R.menu.options, menu);
	    return refreshFooterMenu(menu);
	}
	
	/**
	 * Displays a popup dialog with the list of saved places in it.
	 * Once the user selects a place, the forward places activity is launched.
	 */
	public void showForwardPlaces()
	{
        Builder dialog = new AlertDialog.Builder(new ContextThemeWrapper(activity, android.R.style.Theme_Dialog));
		
		String[] myPlaces = dm.getAllPlaces();
		
		if (0<myPlaces.length)
		{
			ButtonControl forwardPlaces = new ButtonControl(pressTypes.FORWARD_PLACE);
	        dialog.setSingleChoiceItems(myPlaces, -1, forwardPlaces);
	        dialog.setTitle(R.string.forward);
			
	        AlertDialog ad = dialog.create();
	        forwardPlaces.setDialogHandler(ad);
	        ad.show();
		}
		else
		{
			Toast.makeText(activity, R.string.noplaces, Toast.LENGTH_SHORT).show();
		}
	}
	
	/**
	  * This method is called when the Location Screen loads up (or screen orientation changes).
	  * It checks the database for any places which are marked NEW, ie boolean true in DB in column "new".
	  */
	public void showNewPlaces()
	{
		String[] newPlaces = dm.getNewPlaces();
	
		if (0<newPlaces.length)
		{			
			newPlacesDialog = new AlertDialog.Builder(new ContextThemeWrapper(activity, android.R.style.Theme_Dialog));
			//dialog.setInverseBackgroundForced(true);
			newPlacesDialog.setTitle(R.string.newplaces);
			newPlacesDialog.setView(new ListView(activity));
			
			//Popup box built. Now sort out click handlers
			
			ButtonControl placePickControl = new ButtonControl(pressTypes.PICK_PLACE);
			placePickControl.setMultiCheckSize(newPlaces.length);
			newPlacesDialog.setMultiChoiceItems(newPlaces, null, placePickControl);
			
			ButtonControl savePlaces = new ButtonControl(pressTypes.SAVE_PLACES);
			newPlacesDialog.setPositiveButton(R.string.saveSelected, savePlaces);
			
			//And show popup box
			newPlacesDialog.show();
		}
	}

	/**
	 * Used to nullify the dialog so that onBackPressed will function properly.
	 * Otherwise this won't be null, and the screen will be stuck one screen.
	 */
	public void dismissNewPlaces()
	{
		newPlacesDialog=null;	
	}
	/**
	 * When a user clicks on a contact in the contacts list, this method is called.
	 * It displays a popup box with the place name and message body which will be included in 
	 * the SMS body. This box changes depending on whether its in the Location Screen
	 * or in the Forward Screen.
	 * @param contactNums The number(s) associated with the contact.
	 * @param contactNames The name of the contact
	 */
	public void showSendDialog(String[] contactNums, String contactNames)
	{		 
		 Builder dialog = buildSendDialog(true);
		 
 		 ButtonControl sendControl = new ButtonControl(pressTypes.SEND);
 		 sendControl.setNumbersInUse(contactNums);
 		
 	     //Set the title at the top of the popup dialog
 		 String toText; 		 
 		 if(ScreenType.LOCATION == mode)//if(locationMode)
 			toText = activity.getString(R.string.areUShor2Send); 			 
 		 else
 			toText = activity.getString(R.string.forwardTo);

         dialog.setTitle(toText+'\n'+contactNames);
 		 
         //What do we do when the "Send" button is clicked
         dialog.setPositiveButton(R.string.send, sendControl);
         //Using null as the 2nd argument here makes it do nothing, ie Dismiss or Cancel the dialog
         dialog.setNegativeButton(R.string.cancel, null);
              	
         //We don't want to display the "number select" radio buttons if the contact only has 1 phone number
         if(1<contactNums.length)//hide if only one number
 		 {
      		ButtonControl numPickControl = new ButtonControl(pressTypes.PICK_NUM);
      		numPickControl.setNumbersInUse(contactNums);
        	dialog.setSingleChoiceItems(contactNums, 0, numPickControl);
 		 }
         
         dialog.show();
	}


	/**
	 * Shows the "Waiting for GPS" dialog at app start.
	 */
	public void showWaitingForGPS()
	{
		//refreshHeader(activity.getString(R.string.noLoc), R.drawable.quessmal);		 
		 
		//activity.setContentView(mainLayout);
		//When you need to start the dialog box, use this to SHOW it
		//If it has not been built, and is null, this will build it then SHOW it
		myDialog = new ProgressDialog(activity);
		myDialog.setTitle(activity.getString(R.string.checkLoc));
		myDialog.setMessage(activity.getString(R.string.wait));
		myDialog.setIndeterminate(true);
		myDialog.setCancelable(true);
		myDialog.setOnCancelListener(new OnCancelListener()
		{	@Override
			public void onCancel(DialogInterface arg0) 
			{
				activity.finish();
			}
		});
		myDialog.show();
	}

	public void switchToForwardMode()
	{
		switchToForwardMode(ContactTarget.place);
	}

	public void switchToForwardMode(String placeName)
	{
		if(isManageMode())
		{
			setMode(ScreenType.LOCATION);//setMode(true, false, false,false);
			LocationScreen.slideLeft(false);
			restoreScreen();
			
			new CountDownTimer(500, 500)
			{
			     @Override
				public void onFinish()
			     {
			    	 setMode(ScreenType.FORWARD);// setMode(false,true,false,false);
			 		 LocationScreen.slideLeft(true);
			 		 restoreScreen();
			 		 //This will cause a crash if removed.
					 dismissNewPlaces();
			 	 }

			     @Override
				public void onTick(long millisUntilFinished) 
			     {    }
			  }.start();
		}
		else
		{
			setMode(ScreenType.FORWARD);
			restoreScreen();
	 		LocationScreen.slideLeft(true);
		}	
	}
	
	/**
	 * This method loads all the places from the database so the user can edit the name,
	 * view the location on a map, or delete a place.
	 */
	public void switchToManagePlaces()
	{
		String[] myPlaces = dm.getAllPlaces();
		
		if (0<myPlaces.length)
		{		
			if(isForwardMode())
			{
				setMode(ScreenType.LOCATION);
				LocationScreen.slideLeft(true);
				restoreScreen();
				
				
				new CountDownTimer(500, 500)
				{
				     @Override
					public void onFinish()
				     {
				    	 setMode(ScreenType.MANAGE);
				 		 LocationScreen.slideLeft(false);
				    	 restoreScreen();
				 		 //This will cause a crash if removed.
						 dismissNewPlaces();
				 	 }
	
				     @Override
					public void onTick(long millisUntilFinished)
				    {  }
				  }.start();
			}
			else
			{
				setMode(ScreenType.MANAGE);//setMode(false,false,true,false);
				restoreScreen();
		 		LocationScreen.slideLeft(false);
			}
		}
		else
		{
			Toast.makeText(activity, R.string.noplaces, Toast.LENGTH_SHORT).show();
		}
	}
	
	public void switchToMyLocationMode()
	{	
		if(null == newPlacesDialog)
		{
			setMode(ScreenType.LOCATION);
			ContactTarget.clear();
			restoreScreen();
		}
	}
	
	/**
	 * Builds a popup screen asking the user if they'd like to share their "meetup" on a social network.
	 */
	public void buildSocialScreen()
	{
		Builder dialog = social.buildSocialScreen(activity);
		dialog.show();
	}
	
	/**
	 * Initiates a popup for the user to select their preferred medium for sharing the string.
	 * @param stringToShare The string to be posted to twitter/facebook/google+ etc
	 */
	public void shareOnSocial(String stringToShare)
	{
		social.sendToTwitter(stringToShare);	
		ContactTarget.clear();
	}

	/**
	 * Method starts new intent to load the Settings screen activity.
	 */
	public void showSettings()
	{
		setMode(ScreenType.SETTINGS);//setMode(false,false,false,true);
		Intent i = new Intent(activity,SettingsScreen.class);
		i.setFlags(Intent.FLAG_ACTIVITY_PREVIOUS_IS_TOP);
		activity.startActivityForResult(i,0);		
	}

	/**
	 * 
	 * @param colour
	 * @param typeOfPress
	 * @throws IllegalAccessException
	 */
	public void clickColour(int[] colour,pressTypes typeOfPress) throws IllegalAccessException 
	{
		int colourElement=0;
		if (typeOfPress == pressTypes.SETTINGS_CLICK_COLOUR_BOTTOM)
		{colourElement=1;}
		colorPickDialog = new ColorPickerDialog(activity,colour[colourElement]);
		colorPickDialog.setAlphaSliderVisible(false);
	
		colorPickDialog.setButton(activity.getString(R.string.ok), new ButtonControl(typeOfPress,colorPickDialog));
		colorPickDialog.setButton2(activity.getString(R.string.cancel), new ButtonControl(pressTypes.CANCEL_IN_COLORPICKER,colour));
		colorPickDialog.show();
	}

	/**
	 * 
	 * @param color
	 */
	public void colourChange(int color)
	{
		colorPickDialog.onColorChanged(color);
	}

	/**
	 * Called from the Manage Screen. Starts up the browser and bring the user to the iamhe.re
	 * website and displays the location of the selected place.
	 * @param latLon The coordinates in the format "lat,lon"
	 */
	public void showURLinBrowser(String latLon)
	{
		Intent browserIntent = buildMapIntent(latLon);	
		activity.startActivity(browserIntent);
	}

	private Intent buildMapIntent(String latLon)
	{
		String intentURL = "http://"+Driver.getIamhereURL()+latLon;
		//Log.i("iamhere","intentURL is: "+intentURL);
		//String intentURL = "geo:"+theURL;
		Intent mapIntent = new Intent(Intent.ACTION_VIEW, Uri.parse(intentURL));
		return mapIntent;
	}
	
	/**
	 * Method builds a custom chooser dialog with the apps that are able to handle 
	 * HTTP requests and GEO requests, ie a browser, or Google Maps/MapDroyd.
	 * @param coords Location coordinats seperated by a ","
	 */
	public void showViewURLOptions(String coords)
	{
		// Make pop up box
		Dialog appListDialog = new Dialog(activity);
		appListDialog.setContentView(getInstalledGeoApps(coords));
		appListDialog.setTitle("Choose an app to view coords");	
		appListDialog.show();
	}

	/**
	 * Gets the list of apps that can handle the specified intents. Stores them all in a list.
	 * @param lat Latitude  - Used to build the intent URLs
	 * @param lon Longitude - Used to build the intent URLs
	 * @return Returns a ListView containing the apps that can be launched from the intents.
	 */
	private LinearLayout getInstalledGeoApps(String coords)
	{	
		PackageManager packageManager = activity.getPackageManager();      
		//This gets all the browsers
		String browserURI = "http://"+Driver.getIamhereURL()+coords;
		//Log.i("iamhere","browser url is: "+browserURI);
		Intent browserIntent = new Intent(Intent.ACTION_VIEW).setData(Uri.parse(browserURI));

		List<ResolveInfo> browserList = packageManager.queryIntentActivities(browserIntent, 0);
		
		// This gets all the Map apps:
		String mapUri = "geo:"+coords;
		Intent mapIntent = new Intent(Intent.ACTION_VIEW, Uri.parse(mapUri));

		List<ResolveInfo> mapList = packageManager.queryIntentActivities(mapIntent, 0);
		
		//Combine the two
		browserList.addAll(mapList);
		
		//appList.setAdapter(new ArrayAdapter<ResolveInfo>(activity,android.R.layout.simple_list_item_1,browserList));
		LinearLayout appListBody = buildAppList(browserList,packageManager,coords,browserList.size()); 
		
		return appListBody;
	}

	/**
	 * Builds the body of the custom dialog. Populates it with icon and app name.
	 * It also makes the "row" clickable so it can launch the relevant activity
	 * @param listOfApps
	 * @param pm Package Manager
	 * @param coords 
	 * @param sizeOfBrowserList 
	 * @return LinearLayout
	 */
	private LinearLayout buildAppList(List<ResolveInfo> listOfApps, PackageManager pm, String coords, int sizeOfBrowserList)
	{
		LinearLayout appListBody = new LinearLayout(activity);
		LinearLayout  appListRow;// = new LinearLayout(activity);
		ImageView      appImage;//  = new ImageView(activity);
		TextView       appTitle;//  = new TextView(activity);
		appListBody.setOrientation(LinearLayout.VERTICAL);
		String URL = Driver.getIamhereURL();
			
		for(int count=0 ; count<listOfApps.size() ; count++)
		{
			appListRow = new LinearLayout(activity);
			appImage   = new ImageView(activity);
			appTitle   = new TextView(activity);
			
			try{
				appImage.setImageDrawable(pm.getApplicationIcon(listOfApps.get(count).activityInfo.packageName));
			} 
			catch (NameNotFoundException e){
				//Log.i("iamhere","buildAppList::NameNotFoundException::"+e.getMessage());
				e.printStackTrace();
			}
			appTitle.setText(listOfApps.get(count).loadLabel(pm));
			appListRow.addView(appImage);
			appListRow.addView(appTitle);
			appListRow.setTag((count<sizeOfBrowserList?"http://"+URL:"geo:")+coords);
			appListRow.setOnClickListener(new ButtonControl
											(pressTypes.CLICK_GEO_APP_IN_MANAGE
										    ,pm.getLaunchIntentForPackage(listOfApps.get(count).activityInfo.packageName)));
			appListBody.addView(appListRow);
		}
		return appListBody;
	}

	public void startGeoIntent(Intent i, String URL)
	{
		//Log.i("iamhere","The URL is: "+URL);
		Intent launcher = new Intent();
		launcher.putExtras(i);
		launcher.setData(Uri.parse(URL));
		activity.startActivity(launcher);
	}
}