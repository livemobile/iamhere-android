package com.mmtechco.iamhere;

import android.app.Activity;
import android.hardware.Sensor;
import android.hardware.SensorEvent;
import android.hardware.SensorEventListener;
import android.hardware.SensorManager;

public class SensorActivity extends Activity implements SensorEventListener 
{
    private SensorManager 	mSensorManager;
    private Sensor			mOrientation;

    public SensorActivity(Activity activity)
    {
    	super();
    	//Log.i("iamhere","sensor ctr");
        mSensorManager = (SensorManager)activity.getSystemService(SENSOR_SERVICE);
        mOrientation = mSensorManager.getDefaultSensor(Sensor.TYPE_ORIENTATION);
    }
 
    @Override
	protected void onResume()
    {
        super.onResume();
        //Log.i("iamhere","sensor onResume");
        mSensorManager.registerListener(this, mOrientation, SensorManager.SENSOR_DELAY_NORMAL);
    }

    @Override
	protected void onPause()
    {
        super.onPause();
        //Log.i("iamhere","sensor onPause");
        mSensorManager.unregisterListener(this);
    }

    @Override
	public void onAccuracyChanged(Sensor sensor, int accuracy)
    {
    	//Log.i("iamhere","sensor onAccuracyChanged");
    }

    @Override
	public void onSensorChanged(SensorEvent event) 
    {
    	//Log.i("iamhere","sensor onSensorChanged");
    }
}