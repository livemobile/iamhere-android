package com.mmtechco.iamhere.FSM;

import android.app.Activity;
import com.mmtechco.iamhere.contacts.ContactTarget;

public class AppState 
{
	private static AppState thisApp;
	private AppStates previousState;
	private AppStates currentState;
//	private float[]			latLong;
	/*
	private Activity activity;
	
	private Gui				display;
	private LocationMonitor	Locat;
	private Boolean appLoaded;
	*/
	private AppState()
	{
		previousState = AppStates.EXIT_SCREEN;
		currentState  = AppStates.WAITING_GPS;
		//appLoaded = false;
	}
	
	private AppState(Activity _activity)
	{
		this();
		//activity = _activity;
		ContactTarget.clear();
		
	/*	display = Gui.getInstance(this);
		Locat   = LocationMonitor.getinstance(this);
		
		new ButtonControl(this);		*/
		restore(currentState);
	}
	
	public static AppState getInstance(Activity _activity)
	{
		if(null == thisApp)
		{thisApp = new AppState(_activity);}
		else
		{return null;}
		return thisApp;
	}
	
	public boolean setState(AppStates state)
	{
		previousState = currentState;
		
		restore(state);
		return false;
	}
	
	private void restore(AppStates state)
	{
		switch(currentState)
		{
			case CURRENT_PLACE_INFO:
				if(CURRENT_PLACE_INFO())
				currentState = state;
			break;
			case FORWARD_PLACE_SELECTION:
				if(FORWARD_PLACE_SELECTION())
				currentState = state;
			break;
			case FORWARD_PLACE_LIST_CONTACTS:
				currentState = state;
			break;
			case FORWARD_PLACE_INFO:
				currentState = state;
			break;
			case MANAGE_PLACE:
				currentState = state;
			break;
			case ABOUT_SCREEN:
				currentState = state;
			break;
			case SAVE_CURRENT_PLACES:
				currentState = state;
			break;
			case SAVE_NEW_PLACES:
				currentState = state;
			break;
			case MENU_FOOTER:
				currentState = state;
			break;
			case CURRENT_PLACE_LIST_CONTACTS:
				currentState = state;
			break;
			case WAITING_GPS:
				currentState = state;
			break;
			case EXIT_SCREEN:
				currentState = state;
			break;
		}
	}
	
	public void backState()
	{
		switch(previousState)
		{
			case CURRENT_PLACE_INFO:
			break;
			case FORWARD_PLACE_SELECTION:
			break;
			case FORWARD_PLACE_LIST_CONTACTS:
			break;
			case FORWARD_PLACE_INFO:
			break;
			case MANAGE_PLACE:
			break;
			case ABOUT_SCREEN:
			break;
			case SAVE_CURRENT_PLACES:
			break;
			case SAVE_NEW_PLACES:
			break;
			case MENU_FOOTER:
			break;
			case CURRENT_PLACE_LIST_CONTACTS:
			break;
			case WAITING_GPS:
			break;
			case EXIT_SCREEN:
			break;
		}
	}
	
	public AppStates getState()
	{
		return currentState;
	}
	
	private boolean CURRENT_PLACE_INFO()
	{
		return false;
	}
	
	private boolean FORWARD_PLACE_SELECTION()
	{
		return false;
	}
	/*
	private boolean FORWARD_PLACE_LIST_CONTACTS()
	{
		
		return false;
	}
	
	private boolean FORWARD_PLACE_INFO()
	{
		return false;
	}
	
	private boolean MANAGE_PLACE()
	{
		return false;
	}
	
	private boolean ABOUT_SCREEN()
	{
		if(AppStates.SETTINGS_SCREEN == currentState)
		{
			
		}
		return false;
	}
	
	private boolean SAVE_CURRENT_PLACES()
	{
		return false;
	}
	
	private boolean SAVE_NEW_PLACES()
	{
		return false;
	}
	
	private boolean MENU_FOOTER()
	{
		return false;
	}
	
	private boolean CURRENT_PLACE_LIST_CONTACTS()
	{
		boolean result = false;
		if(appLoaded)
		{
			display.switchToMyLocationMode();
			result = true;
		}
		return result;
	}
	
	private boolean WAITING_GPS()
	{		
		float[] latLong = Locat.getGPSNow();
		boolean result = false;
		
		if(0.0 == latLong[0]
		&& 0.0 == latLong[1])
		{	
			activity.setContentView(new MainLayout(activity));
			display.showWaitingForGPS();
			display.checkIfGPSisEnabled();
			Locat.requestUpdates(new CallBack()
			{
				@Override
				public void buildOutContacts(Location loc)
				{
					// TODO Auto-generated method stub
					
				}
			});
		}
		else
		{
			appLoaded = true;
			result 	  = true;
		}
		return result;
	}
	
	private boolean EXIT_SCREEN()
	{
		return false;
	}*/
}