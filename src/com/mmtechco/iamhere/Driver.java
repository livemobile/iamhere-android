package com.mmtechco.iamhere;

import java.util.Locale;

import com.mmtechco.iamhere.contacts.ContactTarget;
import com.mmtechco.iamhere.enums.Region;
import com.mmtechco.iamhere.interfaces.CallBack;
import com.mmtechco.iamhere.screens.MainLayout;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.location.Location;
import android.location.LocationManager;
import android.os.Bundle;
import android.provider.Settings;
import android.widget.Toast;

public class Driver extends Activity implements CallBack
{
	private float[]			latLong;
	private Gui				display;
	private LocationMonitor	Locat;
	private boolean 		contactsBuild; 
	private static String 	URL = "";
	private static Region 	zone;
	
	public static Region getZone()
	{
		/*
		Log.i("iamhere","Country: "+Locale.getDefault().getCountry());
		Log.i("iamhere","Display Language: "+Locale.getDefault().getDisplayLanguage());
		Log.i("iamhere","Lanugage: "+Locale.getDefault().getLanguage());
		Log.i("iamhere","display name: "+Locale.getDefault().getDisplayName());
		Log.i("iamhere","display variant: "+Locale.getDefault().getDisplayVariant());
		*/
		if(null == zone)
		{
			if(Locale.getDefault().getCountry().equalsIgnoreCase("CN"))
			{
				//Log.i("iamhere","Chinese");
				zone = Region.CH;
			}
			else if(Locale.getDefault().getDisplayLanguage().equalsIgnoreCase("en"))
			{//Log.i("iamhere","English");
				zone = Region.EN; 
			}
			else if(Locale.getDefault().getLanguage().equalsIgnoreCase("ar"))
			{// Log.i("iamhere","Arabic");
				zone = Region.AR;
			}
			else
			{//Log.i("iamhere","Default");
				zone = Region.DEFAULT;
			}
		}
		return zone;
	}
	
    public static String getIamhereURL()
    {
    	if(URL.length() == 0)
	    {
			if(Region.CH == zone)
			{	URL = "www.zhelizheli.com/";	}
			else
			{	URL = "www.iamhe.re/";			}
			URL += "find.php?";
    	}
    	
		return URL;
    }
    
	@Override
	public void onCreate(Bundle savedInstanceState) 
	{
		super.onCreate(savedInstanceState);
		//Log.v("iamhere","onCreate");
		getZone();
		
		LocationManager locMan = (LocationManager)getSystemService(Context.LOCATION_SERVICE );
		if(!(null==locMan.getAllProviders()))
		{
			ContactTarget.clear();
			
			display = Gui.getInstance(this);
			Locat   = LocationMonitor.getinstance(this);
			
			new ButtonControl(this);
			contactsBuild = false;
			
			
			if(getLocationOrStart())
			{buildOutContacts();}
			//Run on startup to check the database for previously unhandled places
		//	display.showNewPlaces();
		}
		else
		{
			Toast.makeText(this, getString(R.string.NoGPSInstalled), Toast.LENGTH_LONG).show();
			display.finalize();
		}
	}
	
	@Override
	public void onResume()
	{
		super.onResume();
		//Log.v("iamhere","onResume");
		//getLocationOrStart();		
	}
	
	@Override
	public void onPause()
	{
		super.onPause();
		//Log.v("iamhere","onPause");
		Locat.stopUpdates();
		//display.finalize();
	}
/*
	@Override
	protected void onStop()
	{
		super.onStop();
		Log.v("iamhere","onStop");
		display.finalize();
	}
*/
	
	/**
	 * Queries the device for its current GPS coordinates, and then starts the app proper.
	 * If not GPS coords are found, it'll check if GPS is enabled.
	 */
	private boolean getLocationOrStart()
	{
		latLong = Locat.getGPSNow();
		//Log.i("iamhere","getLocationOrStart:: lat: "+latLong[0]+" and lon: "+latLong[1]);
		 
		Locat.requestUpdates(this);
		
		if(0.0 == latLong[0]
		&& 0.0 == latLong[1])
		{
			//Log.i("iamhere","getLocationOrStart::lat and long are 0");
			setContentView(new MainLayout(this));
			display.showWaitingForGPS();
			display.checkIfGPSisEnabled();
			
			return false;
		}
		return true;
		//buildOutContacts();				
	}

	@Override
	 protected void onActivityResult(int requestCode, int resultCode, Intent data)
	 {
		if(0 == resultCode)
		{
	        String provider = Settings.Secure.getString(getContentResolver(), Settings.Secure.LOCATION_PROVIDERS_ALLOWED);
	        if(provider != null)
			{
	            if(display.checkIfGPSisEnabled())
	            {
	            	getLocationOrStart();
	            }
	        }
	    }
	}
	
	@Override
	 public void buildOutContacts(Location loc)
	 {
		 Locat.setGPS((float)loc.getLatitude(),(float)loc.getLongitude());
		 display.removeWaitingForGPS();
		 if(false == contactsBuild)
		 {
			 buildOutContacts();
		 }
	 }
	 
	 private void buildOutContacts()
	 {
		// Log.v("iamhere","buildOutContacts: "+latLong[0]+","+latLong[1]);
		 display.switchToMyLocationMode();
		 contactsBuild = true;
	 }
}